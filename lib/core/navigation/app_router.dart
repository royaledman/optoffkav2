import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:optoffka/feature/user/presentation/added_tracknumber/screen/add_track_number_page.dart';
import 'package:optoffka/feature/user/presentation/my_delivery_info/screen/my_delivery_info.dart';
import 'package:optoffka/feature/user/presentation/my_receipts/screen/my_receipts_page.dart';
import 'package:optoffka/feature/user/presentation/pages/auth_page.dart';
import 'package:optoffka/feature/user/presentation/banner/screen/home_page.dart';
import 'package:optoffka/feature/user/presentation/my_delivery/screen/my_delivery_page.dart';
import 'package:optoffka/feature/user/presentation/notification/screen/notification_page.dart';
import 'package:optoffka/feature/user/presentation/rule/screen/rule_page.dart';
import 'package:optoffka/feature/user/presentation/search/screen/search_page.dart';
import 'package:optoffka/feature/user/presentation/pages/settings_page.dart';
import 'package:optoffka/feature/user/presentation/secure_rule/screen/secure_rule_page.dart';

part 'app_router.gr.dart';

@AutoRouterConfig(replaceInRouteName: 'Page,Route')
class AppRouter extends _$AppRouter {
  @override
  List<AutoRoute> get routes => [
        AutoRoute(page: AuthRoute.page, initial: true),
        AutoRoute(
          page: HomeRoute.page,
        ),
        AutoRoute(page: SettingRoute.page),
        AutoRoute(page: NotificationRoute.page),
        AutoRoute(page: AddedTrackNumberRoute.page),
        AutoRoute(page: MyDeliveryRoute.page),
        AutoRoute(page: MyDeliveryInfoRoute.page), 
        AutoRoute(page: RuleRoute.page), 
        AutoRoute(page: SearchRoute.page), 
        AutoRoute(page: MyReceiptsRoute.page), 
        AutoRoute(page: SecureRuleRoute.page), 
      ];
}
