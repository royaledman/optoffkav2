// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

part of 'app_router.dart';

abstract class _$AppRouter extends RootStackRouter {
  // ignore: unused_element
  _$AppRouter({super.navigatorKey});

  @override
  final Map<String, PageFactory> pagesMap = {
    AddedTrackNumberRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const AddedTrackNumberPage(),
      );
    },
    AuthRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const AuthPage(),
      );
    },
    HomeRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const HomePage(),
      );
    },
    MyDeliveryInfoRoute.name: (routeData) {
      final args = routeData.argsAs<MyDeliveryInfoRouteArgs>();
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: MyDeliveryInfoPage(
          key: args.key,
          trackId: args.trackId,
        ),
      );
    },
    MyDeliveryRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const MyDeliveryPage(),
      );
    },
    MyReceiptsRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const MyReceiptsPage(),
      );
    },
    NotificationRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const NotificationPage(),
      );
    },
    RuleRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const RulePage(),
      );
    },
    SearchRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const SearchPage(),
      );
    },
    SecureRuleRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const SecureRulePage(),
      );
    },
    SettingRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const SettingPage(),
      );
    },
  };
}

/// generated route for
/// [AddedTrackNumberPage]
class AddedTrackNumberRoute extends PageRouteInfo<void> {
  const AddedTrackNumberRoute({List<PageRouteInfo>? children})
      : super(
          AddedTrackNumberRoute.name,
          initialChildren: children,
        );

  static const String name = 'AddedTrackNumberRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [AuthPage]
class AuthRoute extends PageRouteInfo<void> {
  const AuthRoute({List<PageRouteInfo>? children})
      : super(
          AuthRoute.name,
          initialChildren: children,
        );

  static const String name = 'AuthRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [HomePage]
class HomeRoute extends PageRouteInfo<void> {
  const HomeRoute({List<PageRouteInfo>? children})
      : super(
          HomeRoute.name,
          initialChildren: children,
        );

  static const String name = 'HomeRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [MyDeliveryInfoPage]
class MyDeliveryInfoRoute extends PageRouteInfo<MyDeliveryInfoRouteArgs> {
  MyDeliveryInfoRoute({
    Key? key,
    required int trackId,
    List<PageRouteInfo>? children,
  }) : super(
          MyDeliveryInfoRoute.name,
          args: MyDeliveryInfoRouteArgs(
            key: key,
            trackId: trackId,
          ),
          initialChildren: children,
        );

  static const String name = 'MyDeliveryInfoRoute';

  static const PageInfo<MyDeliveryInfoRouteArgs> page =
      PageInfo<MyDeliveryInfoRouteArgs>(name);
}

class MyDeliveryInfoRouteArgs {
  const MyDeliveryInfoRouteArgs({
    this.key,
    required this.trackId,
  });

  final Key? key;

  final int trackId;

  @override
  String toString() {
    return 'MyDeliveryInfoRouteArgs{key: $key, trackId: $trackId}';
  }
}

/// generated route for
/// [MyDeliveryPage]
class MyDeliveryRoute extends PageRouteInfo<void> {
  const MyDeliveryRoute({List<PageRouteInfo>? children})
      : super(
          MyDeliveryRoute.name,
          initialChildren: children,
        );

  static const String name = 'MyDeliveryRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [MyReceiptsPage]
class MyReceiptsRoute extends PageRouteInfo<void> {
  const MyReceiptsRoute({List<PageRouteInfo>? children})
      : super(
          MyReceiptsRoute.name,
          initialChildren: children,
        );

  static const String name = 'MyReceiptsRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [NotificationPage]
class NotificationRoute extends PageRouteInfo<void> {
  const NotificationRoute({List<PageRouteInfo>? children})
      : super(
          NotificationRoute.name,
          initialChildren: children,
        );

  static const String name = 'NotificationRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [RulePage]
class RuleRoute extends PageRouteInfo<void> {
  const RuleRoute({List<PageRouteInfo>? children})
      : super(
          RuleRoute.name,
          initialChildren: children,
        );

  static const String name = 'RuleRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [SearchPage]
class SearchRoute extends PageRouteInfo<void> {
  const SearchRoute({List<PageRouteInfo>? children})
      : super(
          SearchRoute.name,
          initialChildren: children,
        );

  static const String name = 'SearchRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [SecureRulePage]
class SecureRuleRoute extends PageRouteInfo<void> {
  const SecureRuleRoute({List<PageRouteInfo>? children})
      : super(
          SecureRuleRoute.name,
          initialChildren: children,
        );

  static const String name = 'SecureRuleRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [SettingPage]
class SettingRoute extends PageRouteInfo<void> {
  const SettingRoute({List<PageRouteInfo>? children})
      : super(
          SettingRoute.name,
          initialChildren: children,
        );

  static const String name = 'SettingRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}
