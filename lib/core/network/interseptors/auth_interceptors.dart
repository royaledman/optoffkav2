// import 'package:dio/dio.dart';
// import 'package:flutter_secure_storage/flutter_secure_storage.dart';
// import 'package:optoffka/core/network/dio.dart';
// import 'package:optoffka/feature/auth/data/data_source/user_remote_data_source.dart';

import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:injectable/injectable.dart';
import 'package:optoffka/feature/user/data/data_source/local/user_local_data_source.dart';
import 'package:optoffka/feature/user/data/data_source/remote/user_remote_data_source.dart';

@singleton
class AuthInterceptor extends Interceptor {
  final FlutterSecureStorage _flutterSecureStorage;

  AuthInterceptor({
    required FlutterSecureStorage flutterSecureStorage,
  }) : _flutterSecureStorage = flutterSecureStorage;

  @override
  void onError(DioException err, ErrorInterceptorHandler handler) {
    return handler.next(err);
  }

  @override
  void onRequest(
    RequestOptions options,
    RequestInterceptorHandler handler,
  ) async {
    final token = await _flutterSecureStorage.read(
      key: UserLocalDataSource.token,
    );
    options.headers.addAll(
        {'Authorization': 'Bearer $token', "Accept": "application/json"});

    options.followRedirects = true;
    super.onRequest(options, handler);
  }

  @override
  void onResponse(
    Response response,
    ResponseInterceptorHandler handler,
  ) async {
    if (response.requestOptions.path.contains(
          UserRemoteDataSource.auth,
        ) &&
        response.data['token'] != null) {
      await _flutterSecureStorage.write(
        key: UserLocalDataSource.token,
        value: response.data['token'],
      );
    }

    super.onResponse(response, handler);
  }
}

// class AuthInterceptor extends Interceptor {
//   final _storage = const FlutterSecureStorage(
//     aOptions: AndroidOptions(
//       encryptedSharedPreferences: true,
//     ),
//   );

//   @override
//   void onRequest(
//       RequestOptions options, RequestInterceptorHandler handler) async {
//     options.headers.addAll({
//       "Access-Control-Allow-Origin": "*", // Required for CORS support to work
//       "Access-Control-Allow-Credentials": true,
//     });

//     if (!(options.path.contains(UserRemoteDataSource.authApi) &&
//         options.path.contains(UserRemoteDataSource.refreshApi))) {
//       final accessToken =
//           await _storage.read(key: UserRemoteDataSource.tokenKey);
//       options.headers.addAll({
//         'Authorization': 'Bearer $accessToken',
//       });
//     }

//     return handler.next(options);
//   }

//   @override
//   void onResponse(Response response, ResponseInterceptorHandler handler) async {
//     if (response.requestOptions.path.contains(UserRemoteDataSource.authApi) ||
//         response.requestOptions.path
//             .contains(UserRemoteDataSource.refreshApi)) {
//       await _storage.write(
//         key: UserRemoteDataSource.tokenKey,
//         value: response.data['accessToken'],
//       );

//       await _storage.write(
//         key: UserRemoteDataSource.refreshToken,
//         value: response.data['refreshToken'],
//       );
//     }

//     return handler.next(response);
//   }

//   @override
//   void onError(DioException err, ErrorInterceptorHandler handler) async {
//     if (err.response?.statusCode == 401 &&
//         !err.requestOptions.path.contains(UserRemoteDataSource.authApi)) {
//       final refreshToken =
//           await _storage.read(key: UserRemoteDataSource.refreshToken);
//       DioClient().instance.post(
//             '${UserRemoteDataSource.refreshApi}$refreshToken',
//           );

//       if (err.response != null) {
//         handler.resolve(err.response!);
//       }
//     }

//     if (err.requestOptions.path.contains(UserRemoteDataSource.refreshApi)) {
//       await _storage.delete(
//         key: UserRemoteDataSource.tokenKey,
//       );

//       await _storage.delete(
//         key: UserRemoteDataSource.refreshToken,
//       );
//     }

//     handler.next(err);
//   }
// }