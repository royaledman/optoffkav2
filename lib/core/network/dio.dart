import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:optoffka/core/constants/endpoints.dart';
import 'package:optoffka/core/injection/injectable.dart';
import 'package:optoffka/core/network/interseptors/auth_interceptors.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

@injectable
class DioClient {
  final instance = Dio(
    BaseOptions(
      baseUrl: Endpoints.apiBaseUrl,
      validateStatus: (status)=>(status ?? 0) < 400,
    ),
  )..interceptors.addAll(
      [
        getIt.get<AuthInterceptor>(),
        PrettyDioLogger(
          requestBody: true,
          requestHeader: true,
        ),
      ],
    );
}