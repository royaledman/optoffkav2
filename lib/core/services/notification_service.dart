import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:optoffka/feature/user/data/data_source/local/user_local_data_source.dart';

class NotificationService {
  static FirebaseMessaging messaging = FirebaseMessaging.instance;

  static init() async {
    await messaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );

    String? token = await messaging.getToken();
    if (token != null) {
      const FlutterSecureStorage().write(
        key: UserLocalDataSource.deviceToken,
        value: token,
      );
    }
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      if (message.notification != null) {}
    });
  }
}
