// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:flutter_secure_storage/flutter_secure_storage.dart' as _i4;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

import '../../feature/user/data/data_source/local/user_local_data_source.dart'
    as _i5;
import '../../feature/user/data/data_source/local/user_local_data_source_impl.dart'
    as _i6;
import '../../feature/user/data/data_source/remote/user_remote_data_source.dart'
    as _i7;
import '../../feature/user/data/data_source/remote/user_remote_data_source_impl.dart'
    as _i8;
import '../../feature/user/data/repository/user_repository_impl.dart' as _i10;
import '../../feature/user/domain/repository/user_repository.dart' as _i9;
import '../../feature/user/presentation/added_tracknumber/cubit/added_track_cubit.dart'
    as _i11;
import '../../feature/user/presentation/banner/cubit/banner_cubit.dart' as _i13;
import '../../feature/user/presentation/cubit/cubit/user_cubit.dart' as _i20;
import '../../feature/user/presentation/my_delivery/cubit/my_delivery_cubit.dart'
    as _i15;
import '../../feature/user/presentation/my_delivery_info/cubit/delivery_info_cubit.dart'
    as _i14;
import '../../feature/user/presentation/my_receipts/cubit/receipts_cubit.dart'
    as _i17;
import '../../feature/user/presentation/notification/cubit/notification_cubit.dart'
    as _i16;
import '../../feature/user/presentation/rule/cubit/rule_cubit.dart' as _i18;
import '../../feature/user/presentation/search/cubit/search_cubit.dart' as _i19;
import '../network/dio.dart' as _i3;
import '../network/interseptors/auth_interceptors.dart' as _i12;
import 'modules.dart' as _i21;

extension GetItInjectableX on _i1.GetIt {
// initializes the registration of main-scope dependencies inside of GetIt
  _i1.GetIt init({
    String? environment,
    _i2.EnvironmentFilter? environmentFilter,
  }) {
    final gh = _i2.GetItHelper(
      this,
      environment,
      environmentFilter,
    );
    final registerModules = _$RegisterModules();
    gh.factory<_i3.DioClient>(() => _i3.DioClient());
    gh.factory<_i4.FlutterSecureStorage>(() => registerModules.storage);
    gh.factory<_i5.UserLocalDataSource>(() => _i6.UserLocalDataSourceImpl(
        flutterSecureStorage: gh<_i4.FlutterSecureStorage>()));
    gh.factory<_i7.UserRemoteDataSource>(
        () => _i8.UserRemoteDataSourceImpl(dioClient: gh<_i3.DioClient>()));
    gh.factory<_i9.UserRepository>(() => _i10.UserRepositoryImpl(
          gh<_i7.UserRemoteDataSource>(),
          gh<_i5.UserLocalDataSource>(),
        ));
    gh.factory<_i11.AddedTrackCubit>(
        () => _i11.AddedTrackCubit(gh<_i9.UserRepository>()));
    gh.singleton<_i12.AuthInterceptor>(_i12.AuthInterceptor(
        flutterSecureStorage: gh<_i4.FlutterSecureStorage>()));
    gh.singleton<_i13.BannerCubit>(_i13.BannerCubit(gh<_i9.UserRepository>()));
    gh.factory<_i14.DeliveryInfoCubit>(
        () => _i14.DeliveryInfoCubit(gh<_i9.UserRepository>()));
    gh.factory<_i15.MyDeliveryCubit>(
        () => _i15.MyDeliveryCubit(gh<_i9.UserRepository>()));
    gh.factory<_i16.NotificationCubit>(
        () => _i16.NotificationCubit(gh<_i9.UserRepository>()));
    gh.factory<_i17.ReceiptsCubit>(
        () => _i17.ReceiptsCubit(gh<_i9.UserRepository>()));
    gh.factory<_i18.RuleCubit>(() => _i18.RuleCubit(gh<_i9.UserRepository>()));
    gh.factory<_i19.SearchCubit>(
        () => _i19.SearchCubit(gh<_i9.UserRepository>()));
    gh.singleton<_i20.UserCubit>(_i20.UserCubit(gh<_i9.UserRepository>()));
    return this;
  }
}

class _$RegisterModules extends _i21.RegisterModules {}
