import 'package:auto_route/auto_route.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:optoffka/core/injection/injectable.dart';
import 'package:optoffka/feature/user/presentation/added_tracknumber/cubit/added_track_cubit.dart';
import 'package:optoffka/feature/user/presentation/comman/appbar.dart';
import 'package:optoffka/feature/user/presentation/comman/colors.dart';
import 'package:optoffka/feature/user/presentation/comman/text.dart';

@RoutePage()
class AddedTrackNumberPage extends StatefulWidget {
  const AddedTrackNumberPage({super.key});

  @override
  State<AddedTrackNumberPage> createState() => _AddedTrackNumberPageState();
}

class _AddedTrackNumberPageState extends State<AddedTrackNumberPage> {
  final createTrackCubit = getIt<AddedTrackCubit>();
  TextEditingController trackNumber = TextEditingController();
  TextEditingController description = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.mainColor,
      appBar: const OptoffkaAppbar(),
      body: BlocBuilder<AddedTrackCubit, AddedTrackState>(
        bloc: createTrackCubit,
        builder: (context, state) {
          return state.maybeWhen(
            orElse: () => const Center(
              child: CircularProgressIndicator(),
            ),
            create: () => SingleChildScrollView(
              child: Container(
                margin: const EdgeInsets.only(top: 80),
                child: Column(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Container(
                        margin: const EdgeInsets.only(left: 20),
                        child: const CustomText(
                          text: "Добавить трек номер",
                          textStyle: TextStyle(
                              color: CustomColors.mainPurple,
                              fontWeight: FontWeight.bold,
                              fontSize: 20),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Container(
                        height: 50,
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: CustomColors.mainPurple,
                            width: 2,
                          ),
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: Row(
                          // mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Align(
                              alignment: Alignment.center,
                              child: Container(
                                margin:
                                    const EdgeInsets.only(left: 10, right: 5),
                                child: CustomText(
                                  textStyle: GoogleFonts.montserrat(
                                      color: CustomColors.mainPurple,
                                      fontWeight: FontWeight.w600,
                                      fontSize: 18),
                                  text: "Трек номер",
                                ),
                              ),
                            ),
                            const VerticalDivider(
                              thickness: 3,
                              color: CustomColors.mainPurple,
                              width: 10,
                            ),
                            Align(
                              alignment: Alignment.center,
                              child: SizedBox(
                                // padding: const EdgeInsets.only(top: 18),
                                height: 200,
                                width: 200,
                                child: TextField(
                                  controller: trackNumber,
                                  textInputAction: TextInputAction.done,
                                  style: GoogleFonts.montserrat(
                                    color: CustomColors.mainPurple,
                                    fontSize: 18.0,
                                    fontWeight: FontWeight.w700,
                                  ),
                                  decoration: const InputDecoration(
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.transparent)),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.transparent),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Container(
                        margin: const EdgeInsets.only(left: 20, bottom: 10),
                        child: CustomText(
                          text: "Описание",
                          textStyle: GoogleFonts.montserrat(
                              color: CustomColors.mainPurple,
                              fontWeight: FontWeight.bold,
                              fontSize: 20),
                        ),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      height: 200,
                      child: TextField(
                        controller: description,
                        maxLines: 10,
                        textInputAction: TextInputAction.done,
                        style: const TextStyle(
                            color: CustomColors.mainPurple,
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold),
                        decoration: const InputDecoration(
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 20, horizontal: 20),
                          fillColor: Colors.white,
                          filled: true,
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: CustomColors.mainPurple,
                              width: 2,
                            ),
                            borderRadius: BorderRadius.all(
                              Radius.circular(10),
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: CustomColors.mainPurple, width: 2),
                            borderRadius: BorderRadius.all(
                              Radius.circular(10),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 20),
                      padding: const EdgeInsets.only(top: 10),
                      height: 70,
                      width: 350,
                      child: ElevatedButton.icon(
                        icon: const Icon(
                          Icons.add_rounded,
                          color: CustomColors.mainPurple,
                          size: 40,
                        ),
                        label: CustomText(
                          text: 'Добавить трек номер',
                          maxLines: 1,
                          textStyle: GoogleFonts.montserrat(
                              fontWeight: FontWeight.w600,
                              fontSize: 20,
                              color: CustomColors.mainPurple),
                        ),
                        onPressed: () => {
                          AwesomeDialog(
                            context: context,
                            animType: AnimType.leftSlide,
                            dialogType: DialogType.success,
                            body: const Center(
                              child: Text(
                                "Вы успешно добавили трек номер",
                                style: TextStyle(fontStyle: FontStyle.italic),
                              ),
                            ),
                            btnOkOnPress: () => context.router.pop(),
                          )..show(),
                          createTrackCubit.createTrack(
                            track_number: trackNumber.text,
                            description: description.text,
                          ),
                        },
                        style: ElevatedButton.styleFrom(
                          elevation: 0.0,
                          backgroundColor: CustomColors.mainYellow,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
