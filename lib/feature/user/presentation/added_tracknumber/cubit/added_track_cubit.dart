// ignore_for_file: non_constant_identifier_names

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:optoffka/feature/user/domain/entities/added_tracknumber_dto/added_dto.dart';
import 'package:optoffka/feature/user/domain/repository/user_repository.dart';

part 'added_track_state.dart';
part 'added_track_cubit.freezed.dart';

@injectable
class AddedTrackCubit extends Cubit<AddedTrackState> {
  final UserRepository _userRepository;
  AddedTrackCubit(
    this._userRepository,
  ) : super(const AddedTrackState.create());

  Future<void> createTrack({
    required String track_number,
    required String description,
  }) async {
    try {
      _userRepository.createTrack(
        dto: AddedDto(
          trackNumber: track_number,
          description: description,
        ),
      );
      emit(
        const AddedTrackState.create(),
      );
    } on DioException {
      emit(
        const AddedTrackState.error(),
      );
    }
  }
}
