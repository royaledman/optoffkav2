part of 'added_track_cubit.dart';

@freezed
class AddedTrackState with _$AddedTrackState {
  const factory AddedTrackState.create(
  ) = _Create;
  const factory AddedTrackState.error() = _Error;
}
