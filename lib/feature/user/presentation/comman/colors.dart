
import 'package:flutter/material.dart';

class CustomColors{
  // static const mainColor = Colors.white; 
  static const mainColor = Color(0xFFE1BEE7);
  static const mainYellow = Color.fromARGB(255, 245, 229, 92);
  static const mainPurple = Color.fromARGB(255, 112, 50, 150);
}