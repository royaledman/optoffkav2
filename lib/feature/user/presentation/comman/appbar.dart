import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:optoffka/core/navigation/app_router.dart';
import 'package:optoffka/feature/user/presentation/comman/colors.dart';
import 'package:optoffka/feature/user/presentation/comman/text.dart';

class OptoffkaAppbar extends StatelessWidget implements PreferredSizeWidget {
  const OptoffkaAppbar({super.key});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0,
      leading: IconButton(
        icon: const Icon(
          Icons.more_horiz_rounded,
          color: CustomColors.mainPurple,
        ),
        onPressed: () => context.router.current.name == SettingRoute.name
            ? context.router.pop()
            : context.router.push(
                const SettingRoute(),
              ),
      ),
      backgroundColor: CustomColors.mainColor,
      actions: <Widget>[
        IconButton(
          icon: const Icon(
            Icons.notifications_active,
            color: CustomColors.mainPurple,
          ),
          onPressed: () => context.router.current.name == NotificationRoute.name
              ? context.router.pop()
              : context.router.push(
                  const NotificationRoute(),
                ),
        )
      ],
      centerTitle: true,
      title: GestureDetector(
        onTap: () => context.router.current.name == HomeRoute.name
            ? context.router.pop()
            : context.router.push(
                const HomeRoute(),
              ),
        child: const CustomText(
          text: 'Optoffka',
          textStyle: TextStyle(
            fontFamily: "Lumios-Marker",
            letterSpacing: 3,
            fontSize: 30,
            color: CustomColors.mainPurple,
            fontWeight: FontWeight.w800,
          ),
        ),
      ),
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}
