import 'dart:core';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class CustomText extends StatefulWidget {
  final String text;
  final TextStyle textStyle;
  final int? maxLines;
  final TextOverflow? textOverflow;
  final TextAlign textAlign;
  const CustomText({
    super.key,
    required this.text,
    required this.textStyle,
    this.maxLines,
    this.textOverflow,
    this.textAlign = TextAlign.center,
  });

  factory CustomText.left({
    required String text,
    required TextStyle textStyle,
    int? maxLines,
    TextOverflow? textOverflow,
  }) =>
      CustomText(
        text: text,
        textStyle: textStyle,
        maxLines: maxLines,
        textOverflow: textOverflow,
        textAlign: TextAlign.left,
      );

  @override
  State<CustomText> createState() => _CustomTextState();
}

class _CustomTextState extends State<CustomText> {
  @override
  Widget build(BuildContext context) {
    return AutoSizeText(
      widget.text,
      style: widget.textStyle,
      minFontSize: 12,
      maxLines: widget.maxLines,
      textAlign: widget.textAlign,
      overflow: widget.textOverflow,
    );
  }
}
