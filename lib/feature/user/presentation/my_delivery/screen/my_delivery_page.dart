import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:optoffka/core/injection/injectable.dart';
import 'package:optoffka/core/navigation/app_router.dart';
import 'package:optoffka/feature/user/presentation/comman/appbar.dart';
import 'package:optoffka/feature/user/presentation/comman/colors.dart';
import 'package:optoffka/feature/user/presentation/comman/text.dart';
import 'package:optoffka/feature/user/presentation/my_delivery/cubit/my_delivery_cubit.dart';

@RoutePage()
class MyDeliveryPage extends StatefulWidget {
  const MyDeliveryPage({super.key});

  @override
  State<MyDeliveryPage> createState() => _MyDeliveryPageState();
}

class _MyDeliveryPageState extends State<MyDeliveryPage> {
  final myDeliveryCubit = getIt<MyDeliveryCubit>();

  final _description = TextEditingController();

  @override
  void initState() {
    myDeliveryCubit.getMyDelivery();
    super.initState();
  }

  String? stageId;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: CustomColors.mainColor,
        bottomNavigationBar: SizedBox(
          height: 70,
          width: 380,
          child: ElevatedButton.icon(
            icon: const Icon(
              Icons.add_rounded,
              color: CustomColors.mainPurple,
              size: 40,
            ),
            label: const Text(
              "Добавить трек номер",
              style: TextStyle(color: CustomColors.mainPurple, fontSize: 20),
            ),
            onPressed: () => context.router.push(const AddedTrackNumberRoute()),
            style: ElevatedButton.styleFrom(
              backgroundColor: CustomColors.mainYellow,
              shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30)),
              ),
            ),
          ),
        ),
        appBar: const OptoffkaAppbar(),
        body: BlocBuilder<MyDeliveryCubit, MyDeliveryState>(
          bloc: myDeliveryCubit,
          builder: (context, state) {
            return state.maybeWhen(
              orElse: () => const Center(
                child: CircularProgressIndicator(),
              ),
              loaded: (myDeliveryModel) => SingleChildScrollView(
                child: Container(
                  margin: const EdgeInsets.only(top: 40),
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: TextField(
                          onChanged: (value) {
                            myDeliveryCubit.getMyDelivery(
                              stageId: stageId,
                              description: value,
                            );
                          },
                          controller: _description,
                          decoration: const InputDecoration(
                            hintText: 'Введите описание заказа',
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 10,
                          vertical: 10,
                        ),
                        child: DropdownButtonFormField<String>(
                          onChanged: (value) {
                            myDeliveryCubit.getMyDelivery(
                              stageId: value,
                              description: _description.text,
                            );

                            setState(() {
                              stageId = value;
                            });
                          },
                          isExpanded: true,
                          items: const [
                            DropdownMenuItem(
                              value: null,
                              child: Text('Все'),
                            ),
                            DropdownMenuItem(
                              value: '1',
                              child: Text('Добавлено клиентом'),
                            ),
                            DropdownMenuItem(
                              value: '2',
                              child: Text(
                                  'Поступило в Гуанчжоу, отправлено на таможню'),
                            ),
                            DropdownMenuItem(
                              value: '3',
                              child: Text('Прибыло в Алматы'),
                            ),
                            DropdownMenuItem(
                              value: '4',
                              child: Text('Выдано/отправлено клиенту'),
                            ),
                            DropdownMenuItem(
                              value: '6',
                              child: Text(
                                  'Поврежденный или утерянный на таможне товар'),
                            ),
                          ],
                        ),
                      ),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Container(
                          margin: const EdgeInsets.only(left: 20),
                          child: CustomText(
                            text: "Мои посылки",
                            textStyle: GoogleFonts.montserrat(
                              color: CustomColors.mainPurple,
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                            ),
                          ),
                        ),
                      ),
                      ListView.builder(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemBuilder: (context, index) {
                          return Container(
                            margin: const EdgeInsets.symmetric(
                                horizontal: 20, vertical: 10),
                            decoration: BoxDecoration(
                              color: CustomColors.mainYellow,
                              borderRadius: BorderRadius.circular(15),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(15),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Трек номер : ${myDeliveryModel[index].trackNumber}',
                                    style: const TextStyle(
                                        color: CustomColors.mainPurple,
                                        fontWeight: FontWeight.w700,
                                        fontSize: 17),
                                  ),
                                  const SizedBox(height: 10),
                                  CustomText.left(
                                    text:
                                        'Статус : ${myDeliveryModel[index].stage?.description}',
                                    textStyle: const TextStyle(
                                        color: CustomColors.mainPurple,
                                        fontWeight: FontWeight.w700,
                                        fontSize: 15),
                                  ),
                                  const SizedBox(height: 10),
                                  CustomText.left(
                                    text:
                                        'Дата : ${myDeliveryModel[index].updatedAt}',
                                    textStyle: const TextStyle(
                                        color: CustomColors.mainPurple,
                                        fontWeight: FontWeight.w700,
                                        fontSize: 15),
                                  ),
                                  const SizedBox(height: 10),
                                  CustomText.left(
                                    text:
                                        'Описание : ${myDeliveryModel[index].description ?? 'Нет описания'}',
                                    textStyle: TextStyle(
                                        color: myDeliveryModel[index]
                                                    .description !=
                                                null
                                            ? CustomColors.mainPurple
                                            : Colors.red,
                                        fontWeight: FontWeight.w700,
                                        fontSize: 15),
                                  ),
                                  const SizedBox(height: 10),
                                  TextButton(
                                    onPressed: () async {
                                      await context.router.push(
                                        MyDeliveryInfoRoute(
                                          trackId: myDeliveryModel[index].id,
                                        ),
                                      );
                                      myDeliveryCubit.getMyDelivery(
                                        stageId: stageId,
                                        description: _description.text,
                                      );
                                    },
                                    child: const Text(
                                      'Подробнее',
                                      style: TextStyle(
                                        color: CustomColors.mainPurple,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                        itemCount: myDeliveryModel.length,
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        ));
  }
}
