part of 'my_delivery_cubit.dart';

@freezed
class MyDeliveryState with _$MyDeliveryState {
  const factory MyDeliveryState.loaded({
     required List<MyDeliveryModel> myDeliveryModel,
  }) = _Loaded;
  const factory MyDeliveryState.error() = _Error;
}
