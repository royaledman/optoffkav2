import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:optoffka/feature/user/domain/entities/my_delivery_model/my_delivery_model.dart';
import 'package:optoffka/feature/user/domain/repository/user_repository.dart';

part 'my_delivery_state.dart';
part 'my_delivery_cubit.freezed.dart';

@injectable
class MyDeliveryCubit extends Cubit<MyDeliveryState> {
  final UserRepository _userRepository;
  MyDeliveryCubit(
    this._userRepository,
  ) : super(const MyDeliveryState.error());

  Future<void> getMyDelivery({
    String? stageId,
    String? description,
  }) async {
    try {
      final myDeliveryModel = await _userRepository.getMyDelivery(
        stageId: stageId,
        description: description 
      );
      emit(
        MyDeliveryState.loaded(
          myDeliveryModel: myDeliveryModel,
        ),
      );
    } on DioException {
      emit(
        const MyDeliveryState.error(),
      );
    }
  }
}
