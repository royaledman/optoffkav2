import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:optoffka/core/injection/injectable.dart';
import 'package:optoffka/core/navigation/app_router.dart';
import 'package:optoffka/feature/user/presentation/comman/colors.dart';
import 'package:optoffka/feature/user/presentation/cubit/cubit/user_cubit.dart';
import 'package:url_launcher/url_launcher_string.dart';

@RoutePage()
class AuthPage extends StatefulWidget {
  const AuthPage({super.key});

  @override
  State<AuthPage> createState() => _AuthPageState();
}

class _AuthPageState extends State<AuthPage> {
  @override
  void initState() {
    _auth.getUserToken();
    super.initState();
  }

  TextEditingController login = TextEditingController();
  TextEditingController password = TextEditingController();
  String? errorText;
  final _auth = getIt<UserCubit>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.mainColor,
      body: BlocConsumer<UserCubit, UserState>(
          listener: (context, state) {
            state.whenOrNull(
              onError: (message) => ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: Text(message),
                ),
              ),
              authorized: (userModel) =>
                  context.router.replace(const HomeRoute()),
            );
          },
          bloc: _auth,
          builder: (context, state) {
            return SingleChildScrollView(
              child: Container(
                margin: const EdgeInsets.only(top: 110),
                child: Column(
                  children: <Widget>[
                    Image.asset(
                      'assets/images/mainLogo.png',
                      height: 350,
                      width: 300,
                    ),
                    Container(
                      margin: const EdgeInsets.only(
                        left: 50,
                      ),
                      child: const Stack(
                        children: [
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "Логин",
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.w600,
                                color: CustomColors.mainPurple,
                              ),
                            ),
                          )
                        ],
                      ),
                    ),

                    SizedBox(
                      width: 320,
                      child: Column(
                        children: <Widget>[
                          TextField(
                            controller: login,
                            style: const TextStyle(
                              color: CustomColors.mainPurple,
                              fontSize: 17.0,
                              fontWeight: FontWeight.w700,
                            ),
                            decoration: InputDecoration(
                              errorText: errorText,
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(
                        top: 20,
                        left: 50,
                      ),
                      child: const Stack(
                        children: [
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "Пароль",
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.w600,
                                color: CustomColors.mainPurple,
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 320,
                      // margin: EdgeInsets.only(top: 150),
                      child: Column(
                        children: <Widget>[
                          TextField(
                            controller: password,
                            obscureText: true,
                            style: const TextStyle(
                              color: CustomColors.mainPurple,
                              fontSize: 17.0,
                              fontWeight: FontWeight.w700,
                            ),
                            decoration: InputDecoration(
                              errorText: errorText,
                            ),
                          )
                        ],
                      ),
                    ),
                    // ListView.builder(itemBuilder: (context,index)=>Container(child: products[index]['name'],),itemCount: 200,),
                    Container(
                      margin: const EdgeInsets.only(top: 40),
                      child: ElevatedButton(
                        onPressed: () {
                          state.maybeWhen(
                            orElse: () {
                              _auth.login(
                                login: login.text,
                                password: password.text,
                              );
                            },
                            loading: null,
                          );
                        },
                        style: ElevatedButton.styleFrom(
                            backgroundColor: CustomColors.mainYellow,
                            foregroundColor: CustomColors.mainPurple,
                            shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.circular(14), // <-- Radius
                            ),
                            minimumSize: const Size(220, 60)),
                        child: state.maybeWhen(
                            orElse: () => const Text(
                                  'Войти',
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.w700),
                                ),
                            loading: () => const CircularProgressIndicator()),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 20),
                      child: MaterialButton(
                        onPressed: () {
                          launchUrlString('https://wa.link/w09yzr');
                        },
                        child: const Text(
                          'Не могу войти',
                          style: TextStyle(
                            color: CustomColors.mainPurple,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          }),
    );
  }
}
