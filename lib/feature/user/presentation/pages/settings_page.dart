import 'package:auto_route/auto_route.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:optoffka/core/injection/injectable.dart';
import 'package:optoffka/feature/user/presentation/comman/appbar.dart';
import 'package:optoffka/feature/user/presentation/comman/colors.dart';
import 'package:optoffka/feature/user/presentation/comman/text.dart';
import 'package:optoffka/feature/user/presentation/cubit/cubit/user_cubit.dart';

@RoutePage()
class SettingPage extends StatefulWidget {
  const SettingPage({super.key});

  @override
  State<SettingPage> createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  TextEditingController login = TextEditingController();
  TextEditingController password = TextEditingController();
  TextEditingController city = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController name = TextEditingController();
  final userCubit = getIt<UserCubit>();

  @override
  void didChangeDependencies() {
    userCubit.state.whenOrNull(
      authorized: (userModel) {
        city.text = userModel.city;
        phone.text = userModel.phone;
        name.text = userModel.name;
      },
    );
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.mainColor,
      appBar: const OptoffkaAppbar(),
      body: BlocConsumer<UserCubit, UserState>(
        listener: (context, state) {
          state.whenOrNull(
            onError: (message) => '',
            authorized: (userModel) {
              city.text = userModel.city;
              phone.text = userModel.phone;
              name.text = userModel.name;
            },
            updated: () {
              AwesomeDialog(
                context: context,
                animType: AnimType.leftSlide,
                dialogType: DialogType.success,
                body: const Center(
                  child: Text(
                    "Вы успешно сохранили настройки",
                    style: TextStyle(fontStyle: FontStyle.italic),
                  ),
                ),
                title: 'This is Ignored',
                desc: 'This is also Ignored',
              ).show();
            },
          );
        },
        bloc: userCubit,
        builder: (context, state) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Padding(
                padding: EdgeInsets.only(left: 20),
                child: Text(
                  "Мои настройки",
                  style: TextStyle(
                    color: CustomColors.mainPurple,
                    fontWeight: FontWeight.bold,
                    fontSize: 22,
                  ),
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20),
                child: CustomText.left(
                  text: "Ваш логин - ${state.maybeWhen(
                    orElse: () => '',
                    authorized: (userModel) => userModel.login,
                  )}",
                  textStyle: const TextStyle(
                    color: CustomColors.mainPurple,
                    fontWeight: FontWeight.bold,
                    fontSize: 22,
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 20, bottom: 5, top: 5),
                child: const Text(
                  "Имя",
                  style: TextStyle(
                      color: CustomColors.mainPurple,
                      fontWeight: FontWeight.bold,
                      fontSize: 15),
                ),
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: TextField(
                  controller: name,
                  style: const TextStyle(
                    color: CustomColors.mainPurple,
                    fontSize: 17,
                    fontWeight: FontWeight.w700,
                  ),
                  decoration: const InputDecoration(
                    hintText: "",
                    fillColor: Colors.white,
                    filled: true,
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 20, bottom: 5, top: 5),
                child: const Text(
                  "Город",
                  style: TextStyle(
                    color: CustomColors.mainPurple,
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: TextField(
                  controller: city,
                  style: const TextStyle(
                    color: CustomColors.mainPurple,
                    fontSize: 17.0,
                    fontWeight: FontWeight.w700,
                  ),
                  decoration: const InputDecoration(
                    hintText: "",
                    fillColor: Colors.white,
                    filled: true,
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 20, bottom: 5, top: 5),
                child: const Text(
                  "Номер телефона",
                  style: TextStyle(
                      color: CustomColors.mainPurple,
                      fontWeight: FontWeight.bold,
                      fontSize: 15),
                ),
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: TextField(
                  controller: phone,
                  style: const TextStyle(
                    color: CustomColors.mainPurple,
                    fontSize: 17.0,
                    fontWeight: FontWeight.w700,
                  ),
                  decoration: const InputDecoration(
                    hintText: "",
                    fillColor: Colors.white,
                    filled: true,
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 20, bottom: 5, top: 5),
                child: const Text(
                  "Пароль",
                  style: TextStyle(
                    color: CustomColors.mainPurple,
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: TextField(
                  controller: password,
                  style: const TextStyle(
                    color: CustomColors.mainPurple,
                    fontSize: 17.0,
                    fontWeight: FontWeight.w700,
                  ),
                  decoration: const InputDecoration(
                    hintText: "",
                    fillColor: Colors.white,
                    filled: true,
                  ),
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              Center(
                child: ElevatedButton(
                  onPressed: () => userCubit.updateProfile(
                    name: name.text,
                    password: password.text,
                    city: city.text,
                    phone: phone.text,
                  ),
                  style: ElevatedButton.styleFrom(
                      foregroundColor: CustomColors.mainPurple,
                      backgroundColor: CustomColors.mainYellow,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15), // <-- Radius
                      ),
                      minimumSize:
                          const Size(300, 60)), //<-- Сохранить данные кнопка
                  child: const Text(
                    'Сохранить данные',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.w400),
                  ),
                ),
              ),
              const SizedBox(
                height: 15,
              ), 
              Center(
                child: ElevatedButton(
                  onPressed: () { 
                    getIt.get<UserCubit>().logout();
                  },
                  style: ElevatedButton.styleFrom(
                    foregroundColor: CustomColors.mainPurple,
                    backgroundColor: CustomColors.mainYellow,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15), // <-- Radius
                    ),
                    minimumSize: const Size(300, 60),
                  ),
                  child: const Text(
                    'Выйти',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.w400),
                  ),
                ),
              )
            ],
          );
        },
      ),
    );
  }
}
