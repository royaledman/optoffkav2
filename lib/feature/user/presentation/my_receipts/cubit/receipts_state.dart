part of 'receipts_cubit.dart';

@freezed
class ReceiptsState with _$ReceiptsState {
  const factory ReceiptsState.loaded({
     required List<ReceiptsModel> receiptsModel
  }) = _Loaded;
  const factory ReceiptsState.error() = _Error;
}
