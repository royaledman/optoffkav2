import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:optoffka/feature/user/domain/entities/receipts_model/receipts_model.dart';
import 'package:optoffka/feature/user/domain/repository/user_repository.dart';

part 'receipts_state.dart';
part 'receipts_cubit.freezed.dart';
@injectable
class ReceiptsCubit extends Cubit<ReceiptsState> {
   final UserRepository _userRepository;
  ReceiptsCubit(this._userRepository) : super(const ReceiptsState.error());
  Future<void> getReceipts() async {
    try {
      final receiptsModel = await _userRepository.receiptsModel();
      emit(
       ReceiptsState.loaded(receiptsModel: receiptsModel)
      );
    } on DioException {
      emit(
        const ReceiptsState.error(),
      );
    }
  }
}
