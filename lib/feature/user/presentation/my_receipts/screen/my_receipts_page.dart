import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:optoffka/core/injection/injectable.dart';
import 'package:optoffka/feature/user/presentation/comman/appbar.dart';
import 'package:optoffka/feature/user/presentation/comman/colors.dart';
import 'package:optoffka/feature/user/presentation/comman/text.dart';
import 'package:optoffka/feature/user/presentation/my_receipts/cubit/receipts_cubit.dart';

@RoutePage()
class MyReceiptsPage extends StatefulWidget {
  const MyReceiptsPage({super.key});

  @override
  State<MyReceiptsPage> createState() => _MyReceiptsPageState();
}

class _MyReceiptsPageState extends State<MyReceiptsPage> {
  final receiptsCubit = getIt<ReceiptsCubit>();

  @override
  void initState() {
    receiptsCubit.getReceipts();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.mainColor,
      appBar: const OptoffkaAppbar(),
      body: BlocBuilder<ReceiptsCubit, ReceiptsState>(
        bloc: receiptsCubit,
        builder: (context, state) {
          return state.maybeWhen(
            orElse: () => const Center(
              child: CircularProgressIndicator(),
            ),
            loaded: (receiptsModel) => SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Container(
                      margin:
                          const EdgeInsets.only(left: 10, top: 30, bottom: 10),
                      child: CustomText(
                        text: "Мои поступления ",
                        textStyle: GoogleFonts.montserrat(
                          color: CustomColors.mainPurple,
                          fontWeight: FontWeight.bold,
                          fontSize: 22,
                        ),
                      ),
                    ),
                  ),
                  ListView.builder(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemBuilder: (context, data) {
                      return Container(
                        margin: const EdgeInsets.symmetric(
                            horizontal: 10, vertical: 15),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: CustomColors.mainYellow,
                        ),
                        child: ExpansionTile(
                          expandedCrossAxisAlignment: CrossAxisAlignment.start,
                          expandedAlignment: Alignment.topLeft,
                          childrenPadding:
                              const EdgeInsets.only(left: 15, bottom: 15),
                          backgroundColor: Colors.transparent,
                          title: CustomText.left(
                            text: "Дата - ${receiptsModel[data].date}",
                            textStyle: GoogleFonts.montserrat(
                              color: CustomColors.mainPurple,
                              fontWeight: FontWeight.bold,
                              fontSize: 18,
                            ),
                          ),
                          children: [
                            CustomText.left(
                              text:
                                  "Количество треков - ${receiptsModel[data].quantity}",
                              textStyle: GoogleFonts.montserrat(
                                color: CustomColors.mainPurple,
                                fontWeight: FontWeight.bold,
                                fontSize: 17,
                              ),
                            ),
                            CustomText.left(
                              text: "Вес - ${receiptsModel[data].weight} ",
                              textStyle: GoogleFonts.montserrat(
                                  color: CustomColors.mainPurple,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 17),
                            ),
                            CustomText.left(
                              text:
                                  "Сумма за приход - ${receiptsModel[data].parishSum} тенге",
                              textStyle: GoogleFonts.montserrat(
                                  color: CustomColors.mainPurple,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 17),
                            ),
                          ],
                        ),
                      );
                    },
                    itemCount: receiptsModel.length,
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
