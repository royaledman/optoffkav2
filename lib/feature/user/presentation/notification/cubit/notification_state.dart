part of 'notification_cubit.dart';

@freezed
class NotificationState with _$NotificationState {
  const factory NotificationState.loading(
  {
    required List<NotificationModel> notificationModel,
  }
  ) = _GetNotification;
  const factory NotificationState.error() = _Error;
}
