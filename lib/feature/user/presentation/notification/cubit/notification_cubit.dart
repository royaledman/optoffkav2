import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:optoffka/feature/user/domain/entities/notification_model/notification_model.dart';
import 'package:optoffka/feature/user/domain/repository/user_repository.dart';

part 'notification_state.dart';
part 'notification_cubit.freezed.dart';

@injectable
class NotificationCubit extends Cubit<NotificationState> {
  final UserRepository _userRepository;
  NotificationCubit(
    this._userRepository,
  ) : super(
          const NotificationState.error(),
        );

   Future<void> getNotification() async {
    try {
      final notificationModel = await _userRepository.getNotificationBanner();
      emit(
        NotificationState.loading(
          notificationModel: notificationModel,
        ),
      );
    } on DioException {
      emit(
        const NotificationState.error(),
      );
    }
  }
}
