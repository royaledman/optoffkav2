import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:optoffka/core/injection/injectable.dart';
import 'package:optoffka/core/navigation/app_router.dart';
import 'package:optoffka/feature/user/presentation/comman/appbar.dart';
import 'package:optoffka/feature/user/presentation/comman/colors.dart';
import 'package:optoffka/feature/user/presentation/comman/text.dart';
import 'package:optoffka/feature/user/presentation/notification/cubit/notification_cubit.dart';

@RoutePage()
class NotificationPage extends StatefulWidget {
  const NotificationPage({super.key});

  @override
  State<NotificationPage> createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  final notificationCubit = getIt<NotificationCubit>();

  @override
  void initState() {
    notificationCubit.getNotification();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.mainColor,
      bottomNavigationBar: SizedBox(
        height: 70,
        width: 380,
        child: ElevatedButton.icon(
          icon: const Icon(
            Icons.add_rounded,
            color: CustomColors.mainPurple,
            size: 40,
          ),
          label: const Text(
            "Добавить трек номер",
            style: TextStyle(color: CustomColors.mainPurple, fontSize: 20),
          ),
          onPressed: () => context.router.push(const AddedTrackNumberRoute()),
          style: ElevatedButton.styleFrom(
            backgroundColor: CustomColors.mainYellow,
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30), topRight: Radius.circular(30)),
            ),
          ),
        ),
      ),
      appBar: const OptoffkaAppbar(),
      body: BlocBuilder<NotificationCubit, NotificationState>(
        bloc: notificationCubit,
        builder: (context, state) {
          return state.maybeWhen(
            orElse: () => const Center(
              child: CircularProgressIndicator(),
            ),
            loading: (notificationModel) => SingleChildScrollView(
              child: Container(
                margin: const EdgeInsets.only(top: 20),
                child: Column(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Container(
                        margin: const EdgeInsets.only(left: 20, bottom: 10),
                        child: CustomText(
                          text: "Уведомление",
                          textStyle: GoogleFonts.montserrat(
                              color: CustomColors.mainPurple,
                              fontWeight: FontWeight.bold,
                              fontSize: 20),
                        ),
                      ),
                    ),
                    ListView.builder(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) {
                        return Container(
                          padding: const EdgeInsets.all(5),
                          margin: const EdgeInsets.only(
                            top: 5,
                            right: 15,
                            bottom: 10,
                            left: 15,
                          ),
                          width: 250,
                          decoration: BoxDecoration(
                            border: Border.all(
                                color: CustomColors.mainYellow, width: 4),
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(15),
                          ),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: notificationModel[index]
                                        .trackNumber
                                        .isNotEmpty
                                    ? MainAxisAlignment.spaceBetween
                                    : MainAxisAlignment.end,
                                children: [
                                  if (notificationModel[index]
                                      .trackNumber
                                      .isNotEmpty) ...[
                                    Container(
                                      width: 120,
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                            color: CustomColors.mainYellow,
                                            width: 3),
                                        color: CustomColors.mainYellow,
                                        borderRadius: BorderRadius.circular(15),
                                      ),
                                      child: CustomText(
                                        text: notificationModel[index]
                                            .trackNumber,
                                        textStyle: GoogleFonts.montserrat(
                                            fontSize: 15.5,
                                            color: CustomColors.mainPurple,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ],
                                  Container(
                                    width: 120,
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                            color: CustomColors.mainYellow,
                                            width: 3),
                                        color: CustomColors.mainYellow,
                                        borderRadius:
                                            BorderRadius.circular(15)),
                                    child: CustomText(
                                      text: notificationModel[index].createdAt,
                                      textStyle: GoogleFonts.montserrat(
                                        fontSize: 15.5,
                                        color: CustomColors.mainPurple,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 10, left: 5),
                                child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: CustomText.left(
                                    text: notificationModel[index].description,
                                    textStyle: GoogleFonts.montserrat(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w600,
                                      fontSize: 17,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                      itemCount: notificationModel.length,
                    )
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
