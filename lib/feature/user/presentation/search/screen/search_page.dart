import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:optoffka/core/injection/injectable.dart';
import 'package:optoffka/feature/user/presentation/comman/appbar.dart';
import 'package:optoffka/feature/user/presentation/comman/colors.dart';
import 'package:optoffka/feature/user/presentation/search/cubit/search_cubit.dart';

@RoutePage()
class SearchPage extends StatefulWidget {
  const SearchPage({super.key});

  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  List<dynamic> searching = [];
  TextEditingController search = TextEditingController();

  final _searchCubit = getIt.get<SearchCubit>();

  @override
  void initState() {
    _searchCubit.searchTrack();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.mainColor,
      appBar: const OptoffkaAppbar(),
      body: BlocBuilder<SearchCubit, SearchState>(
        bloc: _searchCubit,
        builder: (context, state) {
          return state.maybeWhen(
            orElse: () => const Center(
              child: CircularProgressIndicator(),
            ),
            loaded: (tracks) => Container(
              padding: const EdgeInsets.only(top: 40),
              child: Column(
                children: [
                  Container(
                    margin:
                        const EdgeInsets.only(left: 20, right: 20, bottom: 20),
                    child: TextField(
                      controller: search,
                      onChanged: (value) {
                        _searchCubit.searchTrack(search: value);
                      },
                      style: const TextStyle(color: Colors.black),
                      decoration: const InputDecoration(
                        contentPadding: EdgeInsets.only(top: 5, bottom: 5),
                        fillColor: Colors.white,
                        filled: true,
                        prefixIcon: Icon(
                          Icons.search,
                          size: 30,
                          color: CustomColors.mainPurple,
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: ListView.builder(
                      itemBuilder: (context, index) {
                        return Container(
                          margin: const EdgeInsets.only(
                              bottom: 10, left: 20, right: 20),
                          child: Container(
                            padding: const EdgeInsets.all(5),
                            decoration: BoxDecoration(
                                border:
                                    Border.all(color: Colors.yellow, width: 3),
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(15)),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      margin: const EdgeInsets.only(
                                        // right: 30,
                                        bottom: 10,
                                      ),
                                      width: 120,
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              color: Colors.yellow, width: 3),
                                          color: Colors.yellow,
                                          borderRadius:
                                              BorderRadius.circular(15)),
                                      child: Text(
                                        tracks[index].createdAt ?? '',
                                        textAlign: TextAlign.center,
                                        style: const TextStyle(
                                            fontSize: 15.5,
                                            color: Colors.deepPurple,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    Container(
                                      margin: const EdgeInsets.only(
                                        // left: 10,
                                        bottom: 10,
                                      ),
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              color: Colors.yellow, width: 3),
                                          color: Colors.yellow,
                                          borderRadius:
                                              BorderRadius.circular(15)),
                                      child: Text(
                                        "# ${tracks[index].trackNumber}  ",
                                        textAlign: TextAlign.center,
                                        style: const TextStyle(
                                            fontSize: 15.5,
                                            color: Colors.deepPurple,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    const Icon(
                                      Icons.check_circle,
                                      color: CustomColors.mainPurple,
                                      size: 40.0,
                                    ),
                                    const SizedBox(
                                      width: 20,
                                    ),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            tracks[index].stage != null
                                                ? tracks[index]
                                                        .stage!
                                                        .description ??
                                                    ''
                                                : '',
                                            style: const TextStyle(
                                                color: CustomColors.mainPurple,
                                                fontWeight: FontWeight.w600,
                                                fontSize: 15),
                                          ),
                                          Text(
                                            'Описание трека: ${tracks[index].description ?? ''}',
                                            style: const TextStyle(
                                                color: CustomColors.mainPurple,
                                                fontWeight: FontWeight.w600,
                                                fontSize: 15),
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                      itemCount: tracks.length,
                    ),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
