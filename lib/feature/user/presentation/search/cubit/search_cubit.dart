import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:optoffka/feature/user/domain/entities/my_delivery_model/my_delivery_model.dart';
import 'package:optoffka/feature/user/domain/repository/user_repository.dart';

part 'search_state.dart';
part 'search_cubit.freezed.dart';

@injectable
class SearchCubit extends Cubit<SearchState> {
  final UserRepository _userRepository;

  SearchCubit(this._userRepository) : super(const SearchState.initial());

  void searchTrack({String search = ''}) async {
    try {
      final numbers = await _userRepository.searchTrackNumber(
        search: search,
      );

      emit(SearchState.loaded(numbers));
    } on DioException {
      emit(const SearchState.initial());
    }
  }
}
