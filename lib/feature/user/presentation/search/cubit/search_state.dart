part of 'search_cubit.dart';

@freezed
class SearchState with _$SearchState {
  const factory SearchState.initial() = _Initial;
  const factory SearchState.loaded(
    List<MyDeliveryModel> tracks,
  ) = _Loaded;
}
