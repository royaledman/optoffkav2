import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:optoffka/feature/user/domain/entities/history_model/history_model.dart';
import 'package:optoffka/feature/user/domain/entities/my_delivery_info_model/my_delivery_info_model.dart';
import 'package:optoffka/feature/user/domain/repository/user_repository.dart';

part 'delivery_info_state.dart';
part 'delivery_info_cubit.freezed.dart';

@injectable
class DeliveryInfoCubit extends Cubit<DeliveryInfoState> {
  DeliveryInfoCubit(this._userRepository)
      : super(const DeliveryInfoState.initial());

  final UserRepository _userRepository;

  void getDeliveryInfo({
    required int trackId,
    required String ship,
  }) async {
    try {
      final deliveryModel =
          await _userRepository.getMyDeliveryInfo(trackId: trackId);

      final historyModel = await _userRepository.getHistory(ship: ship);

      emit(
        DeliveryInfoState.loaded(
          myDeliveryInfoModel: deliveryModel,
          historyModel: historyModel,
        ),
      );
    } on DioException {
      emit(
        const DeliveryInfoState.initial(),
      );
    }
  }

  void deleteDeliveryInfo({required int trackId}) async {
    try {
      await _userRepository.deleteDelivery(trackId: trackId);

      emit(const DeliveryInfoState.deleted());
    } on DioException {
      emit(
        const DeliveryInfoState.initial(),
      );
    }
  }

  void editShip({
    required int shipId,
    required String description,
  }) async {
    try {
      await _userRepository.updateShipDescription(
        shipId: shipId,
        description: description,
      );
      emit(const DeliveryInfoState.updated());

      getDeliveryInfo(trackId: shipId, ship: "$shipId");
    } on DioException {
      emit(const DeliveryInfoState.initial());
    }
  }

  void historyTrack({
    required String ship,
    required int trackId,
  }) async {
    final deliveryModel =
        await _userRepository.getMyDeliveryInfo(trackId: trackId);

    try {
      final historyModel = await _userRepository.getHistory(
        ship: ship,
      );

      emit(
        DeliveryInfoState.loaded(
          myDeliveryInfoModel: deliveryModel,
          historyModel: historyModel,
        ),
      );
    } on DioException {
      emit(
        const DeliveryInfoState.initial(),
      );
    }
  }
}
