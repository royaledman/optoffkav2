part of 'delivery_info_cubit.dart';

@freezed
class DeliveryInfoState with _$DeliveryInfoState {
  const factory DeliveryInfoState.initial() = _Initial;
  const factory DeliveryInfoState.deleted() = _Deleted;
  const factory DeliveryInfoState.updated() = _Updated;
  const factory DeliveryInfoState.loaded({
    required MyDeliveryInfoModel myDeliveryInfoModel,
    required HistoryModel historyModel,
  }) = _Loaded;


}
 