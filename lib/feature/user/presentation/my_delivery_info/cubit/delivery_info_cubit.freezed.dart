// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'delivery_info_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$DeliveryInfoState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() deleted,
    required TResult Function() updated,
    required TResult Function(
            MyDeliveryInfoModel myDeliveryInfoModel, HistoryModel historyModel)
        loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? deleted,
    TResult? Function()? updated,
    TResult? Function(
            MyDeliveryInfoModel myDeliveryInfoModel, HistoryModel historyModel)?
        loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? deleted,
    TResult Function()? updated,
    TResult Function(
            MyDeliveryInfoModel myDeliveryInfoModel, HistoryModel historyModel)?
        loaded,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Deleted value) deleted,
    required TResult Function(_Updated value) updated,
    required TResult Function(_Loaded value) loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Deleted value)? deleted,
    TResult? Function(_Updated value)? updated,
    TResult? Function(_Loaded value)? loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Deleted value)? deleted,
    TResult Function(_Updated value)? updated,
    TResult Function(_Loaded value)? loaded,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DeliveryInfoStateCopyWith<$Res> {
  factory $DeliveryInfoStateCopyWith(
          DeliveryInfoState value, $Res Function(DeliveryInfoState) then) =
      _$DeliveryInfoStateCopyWithImpl<$Res, DeliveryInfoState>;
}

/// @nodoc
class _$DeliveryInfoStateCopyWithImpl<$Res, $Val extends DeliveryInfoState>
    implements $DeliveryInfoStateCopyWith<$Res> {
  _$DeliveryInfoStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$InitialImplCopyWith<$Res> {
  factory _$$InitialImplCopyWith(
          _$InitialImpl value, $Res Function(_$InitialImpl) then) =
      __$$InitialImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InitialImplCopyWithImpl<$Res>
    extends _$DeliveryInfoStateCopyWithImpl<$Res, _$InitialImpl>
    implements _$$InitialImplCopyWith<$Res> {
  __$$InitialImplCopyWithImpl(
      _$InitialImpl _value, $Res Function(_$InitialImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$InitialImpl implements _Initial {
  const _$InitialImpl();

  @override
  String toString() {
    return 'DeliveryInfoState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$InitialImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() deleted,
    required TResult Function() updated,
    required TResult Function(
            MyDeliveryInfoModel myDeliveryInfoModel, HistoryModel historyModel)
        loaded,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? deleted,
    TResult? Function()? updated,
    TResult? Function(
            MyDeliveryInfoModel myDeliveryInfoModel, HistoryModel historyModel)?
        loaded,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? deleted,
    TResult Function()? updated,
    TResult Function(
            MyDeliveryInfoModel myDeliveryInfoModel, HistoryModel historyModel)?
        loaded,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Deleted value) deleted,
    required TResult Function(_Updated value) updated,
    required TResult Function(_Loaded value) loaded,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Deleted value)? deleted,
    TResult? Function(_Updated value)? updated,
    TResult? Function(_Loaded value)? loaded,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Deleted value)? deleted,
    TResult Function(_Updated value)? updated,
    TResult Function(_Loaded value)? loaded,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements DeliveryInfoState {
  const factory _Initial() = _$InitialImpl;
}

/// @nodoc
abstract class _$$DeletedImplCopyWith<$Res> {
  factory _$$DeletedImplCopyWith(
          _$DeletedImpl value, $Res Function(_$DeletedImpl) then) =
      __$$DeletedImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$DeletedImplCopyWithImpl<$Res>
    extends _$DeliveryInfoStateCopyWithImpl<$Res, _$DeletedImpl>
    implements _$$DeletedImplCopyWith<$Res> {
  __$$DeletedImplCopyWithImpl(
      _$DeletedImpl _value, $Res Function(_$DeletedImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$DeletedImpl implements _Deleted {
  const _$DeletedImpl();

  @override
  String toString() {
    return 'DeliveryInfoState.deleted()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$DeletedImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() deleted,
    required TResult Function() updated,
    required TResult Function(
            MyDeliveryInfoModel myDeliveryInfoModel, HistoryModel historyModel)
        loaded,
  }) {
    return deleted();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? deleted,
    TResult? Function()? updated,
    TResult? Function(
            MyDeliveryInfoModel myDeliveryInfoModel, HistoryModel historyModel)?
        loaded,
  }) {
    return deleted?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? deleted,
    TResult Function()? updated,
    TResult Function(
            MyDeliveryInfoModel myDeliveryInfoModel, HistoryModel historyModel)?
        loaded,
    required TResult orElse(),
  }) {
    if (deleted != null) {
      return deleted();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Deleted value) deleted,
    required TResult Function(_Updated value) updated,
    required TResult Function(_Loaded value) loaded,
  }) {
    return deleted(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Deleted value)? deleted,
    TResult? Function(_Updated value)? updated,
    TResult? Function(_Loaded value)? loaded,
  }) {
    return deleted?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Deleted value)? deleted,
    TResult Function(_Updated value)? updated,
    TResult Function(_Loaded value)? loaded,
    required TResult orElse(),
  }) {
    if (deleted != null) {
      return deleted(this);
    }
    return orElse();
  }
}

abstract class _Deleted implements DeliveryInfoState {
  const factory _Deleted() = _$DeletedImpl;
}

/// @nodoc
abstract class _$$UpdatedImplCopyWith<$Res> {
  factory _$$UpdatedImplCopyWith(
          _$UpdatedImpl value, $Res Function(_$UpdatedImpl) then) =
      __$$UpdatedImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$UpdatedImplCopyWithImpl<$Res>
    extends _$DeliveryInfoStateCopyWithImpl<$Res, _$UpdatedImpl>
    implements _$$UpdatedImplCopyWith<$Res> {
  __$$UpdatedImplCopyWithImpl(
      _$UpdatedImpl _value, $Res Function(_$UpdatedImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$UpdatedImpl implements _Updated {
  const _$UpdatedImpl();

  @override
  String toString() {
    return 'DeliveryInfoState.updated()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$UpdatedImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() deleted,
    required TResult Function() updated,
    required TResult Function(
            MyDeliveryInfoModel myDeliveryInfoModel, HistoryModel historyModel)
        loaded,
  }) {
    return updated();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? deleted,
    TResult? Function()? updated,
    TResult? Function(
            MyDeliveryInfoModel myDeliveryInfoModel, HistoryModel historyModel)?
        loaded,
  }) {
    return updated?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? deleted,
    TResult Function()? updated,
    TResult Function(
            MyDeliveryInfoModel myDeliveryInfoModel, HistoryModel historyModel)?
        loaded,
    required TResult orElse(),
  }) {
    if (updated != null) {
      return updated();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Deleted value) deleted,
    required TResult Function(_Updated value) updated,
    required TResult Function(_Loaded value) loaded,
  }) {
    return updated(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Deleted value)? deleted,
    TResult? Function(_Updated value)? updated,
    TResult? Function(_Loaded value)? loaded,
  }) {
    return updated?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Deleted value)? deleted,
    TResult Function(_Updated value)? updated,
    TResult Function(_Loaded value)? loaded,
    required TResult orElse(),
  }) {
    if (updated != null) {
      return updated(this);
    }
    return orElse();
  }
}

abstract class _Updated implements DeliveryInfoState {
  const factory _Updated() = _$UpdatedImpl;
}

/// @nodoc
abstract class _$$LoadedImplCopyWith<$Res> {
  factory _$$LoadedImplCopyWith(
          _$LoadedImpl value, $Res Function(_$LoadedImpl) then) =
      __$$LoadedImplCopyWithImpl<$Res>;
  @useResult
  $Res call(
      {MyDeliveryInfoModel myDeliveryInfoModel, HistoryModel historyModel});

  $MyDeliveryInfoModelCopyWith<$Res> get myDeliveryInfoModel;
  $HistoryModelCopyWith<$Res> get historyModel;
}

/// @nodoc
class __$$LoadedImplCopyWithImpl<$Res>
    extends _$DeliveryInfoStateCopyWithImpl<$Res, _$LoadedImpl>
    implements _$$LoadedImplCopyWith<$Res> {
  __$$LoadedImplCopyWithImpl(
      _$LoadedImpl _value, $Res Function(_$LoadedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? myDeliveryInfoModel = null,
    Object? historyModel = null,
  }) {
    return _then(_$LoadedImpl(
      myDeliveryInfoModel: null == myDeliveryInfoModel
          ? _value.myDeliveryInfoModel
          : myDeliveryInfoModel // ignore: cast_nullable_to_non_nullable
              as MyDeliveryInfoModel,
      historyModel: null == historyModel
          ? _value.historyModel
          : historyModel // ignore: cast_nullable_to_non_nullable
              as HistoryModel,
    ));
  }

  @override
  @pragma('vm:prefer-inline')
  $MyDeliveryInfoModelCopyWith<$Res> get myDeliveryInfoModel {
    return $MyDeliveryInfoModelCopyWith<$Res>(_value.myDeliveryInfoModel,
        (value) {
      return _then(_value.copyWith(myDeliveryInfoModel: value));
    });
  }

  @override
  @pragma('vm:prefer-inline')
  $HistoryModelCopyWith<$Res> get historyModel {
    return $HistoryModelCopyWith<$Res>(_value.historyModel, (value) {
      return _then(_value.copyWith(historyModel: value));
    });
  }
}

/// @nodoc

class _$LoadedImpl implements _Loaded {
  const _$LoadedImpl(
      {required this.myDeliveryInfoModel, required this.historyModel});

  @override
  final MyDeliveryInfoModel myDeliveryInfoModel;
  @override
  final HistoryModel historyModel;

  @override
  String toString() {
    return 'DeliveryInfoState.loaded(myDeliveryInfoModel: $myDeliveryInfoModel, historyModel: $historyModel)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LoadedImpl &&
            (identical(other.myDeliveryInfoModel, myDeliveryInfoModel) ||
                other.myDeliveryInfoModel == myDeliveryInfoModel) &&
            (identical(other.historyModel, historyModel) ||
                other.historyModel == historyModel));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, myDeliveryInfoModel, historyModel);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$LoadedImplCopyWith<_$LoadedImpl> get copyWith =>
      __$$LoadedImplCopyWithImpl<_$LoadedImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() deleted,
    required TResult Function() updated,
    required TResult Function(
            MyDeliveryInfoModel myDeliveryInfoModel, HistoryModel historyModel)
        loaded,
  }) {
    return loaded(myDeliveryInfoModel, historyModel);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? deleted,
    TResult? Function()? updated,
    TResult? Function(
            MyDeliveryInfoModel myDeliveryInfoModel, HistoryModel historyModel)?
        loaded,
  }) {
    return loaded?.call(myDeliveryInfoModel, historyModel);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? deleted,
    TResult Function()? updated,
    TResult Function(
            MyDeliveryInfoModel myDeliveryInfoModel, HistoryModel historyModel)?
        loaded,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(myDeliveryInfoModel, historyModel);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Deleted value) deleted,
    required TResult Function(_Updated value) updated,
    required TResult Function(_Loaded value) loaded,
  }) {
    return loaded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Deleted value)? deleted,
    TResult? Function(_Updated value)? updated,
    TResult? Function(_Loaded value)? loaded,
  }) {
    return loaded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Deleted value)? deleted,
    TResult Function(_Updated value)? updated,
    TResult Function(_Loaded value)? loaded,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(this);
    }
    return orElse();
  }
}

abstract class _Loaded implements DeliveryInfoState {
  const factory _Loaded(
      {required final MyDeliveryInfoModel myDeliveryInfoModel,
      required final HistoryModel historyModel}) = _$LoadedImpl;

  MyDeliveryInfoModel get myDeliveryInfoModel;
  HistoryModel get historyModel;
  @JsonKey(ignore: true)
  _$$LoadedImplCopyWith<_$LoadedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
