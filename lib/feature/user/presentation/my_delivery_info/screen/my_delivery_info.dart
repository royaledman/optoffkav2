import 'package:auto_route/auto_route.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:optoffka/core/injection/injectable.dart';
import 'package:optoffka/feature/user/presentation/comman/appbar.dart';
import 'package:optoffka/feature/user/presentation/comman/colors.dart';
import 'package:optoffka/feature/user/presentation/comman/text.dart';
import 'package:optoffka/feature/user/presentation/my_delivery_info/cubit/delivery_info_cubit.dart';

@RoutePage()
class MyDeliveryInfoPage extends StatefulWidget {
  final int trackId;
  const MyDeliveryInfoPage({
    super.key,
    required this.trackId,
  });

  @override
  State<MyDeliveryInfoPage> createState() => _MyDeliveryInfoPageState();
}

extension ToStringDate on DateTime {
  String formatDate() {
    try {
      return "${day.toString().padLeft(2, '0')}.${month.toString().padLeft(2, '0')}.$year";
    } catch (e) {
      // В случае ошибки во звращаем исходную строку
      return toString();
    }
  }
}

class _MyDeliveryInfoPageState extends State<MyDeliveryInfoPage> {
  final _deliveryInfoCubit = getIt.get<DeliveryInfoCubit>();
  final _description = TextEditingController();

  @override
  void initState() {
    _deliveryInfoCubit.getDeliveryInfo(
      trackId: widget.trackId,
      ship: "${widget.trackId}",
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.mainColor,
      appBar: const OptoffkaAppbar(),
      body: SingleChildScrollView(
        child: BlocConsumer<DeliveryInfoCubit, DeliveryInfoState>(
          listener: (context, state) {
            state.whenOrNull(
                deleted: () => context.router.pop(),
                loaded: (
                  myDeliveryInfoModel,
                  historyModel,
                ) {
                  _description.text = myDeliveryInfoModel.description ?? '';
                  setState(() {});
                },
                updated: () {
                  AwesomeDialog(
                    context: context,
                    animType: AnimType.leftSlide,
                    dialogType: DialogType.success,
                    body: const Center(
                      child: Text(
                        "Изменения сохранены",
                        style: TextStyle(fontStyle: FontStyle.italic),
                      ),
                    ),
                    btnOkOnPress: () => context.router.pop(),
                  ).show();
                });
          },
          bloc: _deliveryInfoCubit,
          builder: (context, state) {
            return state.maybeWhen(
              orElse: () => const Center(
                child: CircularProgressIndicator(),
              ),
              loaded: (
                myDeliveryInfoModel,
                historyModel,
              ) =>
                  Container(
                margin: const EdgeInsets.only(top: 80),
                child: Column(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Container(
                        margin: const EdgeInsets.only(left: 20),
                        child: CustomText(
                          text: "Трек номер",
                          textStyle: GoogleFonts.montserrat(
                            color: CustomColors.mainPurple,
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(
                          top: 20, right: 20, bottom: 30, left: 20),
                      height: 50,
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: CustomColors.mainPurple,
                          width: 2,
                        ),
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Row(
                        children: [
                          Align(
                            alignment: Alignment.center,
                            child: Container(
                              margin: const EdgeInsets.all(5),
                              // height: 200,
                              child: CustomText(
                                text: myDeliveryInfoModel.trackNumber,
                                textStyle: GoogleFonts.montserrat(
                                    color: CustomColors.mainPurple,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Container(
                        margin: const EdgeInsets.only(left: 20),
                        child: CustomText(
                          text: "Хронология трек номера",
                          textStyle: GoogleFonts.montserrat(
                            color: CustomColors.mainPurple,
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(
                          top: 20, right: 20, bottom: 30, left: 20),
                      // height: 50,
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: CustomColors.mainPurple,
                          width: 2,
                        ),
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Column(
                        children: [
                          if (historyModel.created_at != null)
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                margin: const EdgeInsets.only(
                                    left: 20, bottom: 10, top: 10),
                                child: CustomText(
                                  text:
                                      "Добавлено клиентом:  ${historyModel.created_at?.toLocal().formatDate() ?? ''}",
                                  textStyle: GoogleFonts.montserrat(
                                    color: CustomColors.mainPurple,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15,
                                  ),
                                ),
                              ),
                            ),
                          if (historyModel.second_stage_date != null)
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                margin:
                                    const EdgeInsets.only(left: 20, bottom: 10),
                                child: CustomText.left(
                                  text:
                                      "Поступило в Гуанчжоу, отправлено на таможню:   ${historyModel.second_stage_date?.toLocal().formatDate()}",
                                  textStyle: GoogleFonts.montserrat(
                                    color: CustomColors.mainPurple,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15,
                                  ),
                                ),
                              ),
                            ),
                          if (historyModel.third_stage_date != null)
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                margin:
                                    const EdgeInsets.only(left: 20, bottom: 10),
                                child: CustomText.left(
                                  text:
                                      "Прибыло в Алматы:    ${historyModel.third_stage_date?.toLocal().formatDate()}",
                                  textStyle: GoogleFonts.montserrat(
                                    color: CustomColors.mainPurple,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15,
                                  ),
                                ),
                              ),
                            ),
                          if (historyModel.fourth_stage_date != null)
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                margin:
                                    const EdgeInsets.only(left: 20, bottom: 10),
                                child: CustomText.left(
                                  text:
                                      "Выдано/отправлено клиенту:    ${historyModel.fourth_stage_date?.toLocal().formatDate()}",
                                  textStyle: GoogleFonts.montserrat(
                                    color: CustomColors.mainPurple,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15,
                                  ),
                                ),
                              ),
                            ),
                          if (historyModel.fifth_stage_date != null)
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                margin:
                                    const EdgeInsets.only(left: 20, bottom: 10),
                                child: CustomText.left(
                                  text:
                                      "Поврежденный или утерянный на таможне товар:   ${historyModel.fifth_stage_date?.toLocal().formatDate()}",
                                  textStyle: GoogleFonts.montserrat(
                                    color: CustomColors.mainPurple,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15,
                                  ),
                                ),
                              ),
                            ),
                        ],
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Container(
                        margin: const EdgeInsets.only(left: 20, bottom: 10),
                        child: CustomText(
                          text: "Описание",
                          textStyle: GoogleFonts.montserrat(
                            color: CustomColors.mainPurple,
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 20,
                      ),
                      height: 200,
                      child: TextField(
                        controller: _description,
                        maxLines: 10,
                        style: const TextStyle(
                            color: CustomColors.mainPurple,
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold),
                        decoration: InputDecoration(
                          contentPadding: const EdgeInsets.symmetric(
                              vertical: 20, horizontal: 20),
                          fillColor: Colors.white,
                          filled: true,
                          hintText: 'Добавьте описание',
                          enabledBorder: const OutlineInputBorder(
                            borderSide: BorderSide(
                              color: CustomColors.mainPurple,
                              width: 2,
                            ),
                            borderRadius: BorderRadius.all(
                              Radius.circular(10),
                            ),
                          ),
                          hintStyle: TextStyle(
                              color: CustomColors.mainPurple.withOpacity(0.5),
                              fontSize: 17.0),
                          focusedBorder: const OutlineInputBorder(
                            borderSide: BorderSide(
                                color: CustomColors.mainPurple, width: 2),
                            borderRadius: BorderRadius.all(
                              Radius.circular(10),
                            ),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    SizedBox(
                      height: 60,
                      width: 300,
                      child: ElevatedButton(
                        onPressed: () {
                          _deliveryInfoCubit.editShip(
                            shipId: widget.trackId,
                            description: _description.text,
                          );
                        },
                        style: ElevatedButton.styleFrom(
                          elevation: 0.0,
                          backgroundColor: CustomColors.mainYellow,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                        ),
                        child: CustomText(
                          text: "Изменить описание",
                          textStyle: GoogleFonts.montserrat(
                            color: CustomColors.mainPurple,
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    SizedBox(
                      height: 60,
                      width: 300,
                      child: ElevatedButton(
                        onPressed: () {
                          AwesomeDialog(
                            context: context,
                            dialogType: DialogType.infoReverse,
                            headerAnimationLoop: true,
                            animType: AnimType.topSlide,
                            showCloseIcon: true,
                            closeIcon:
                                const Icon(Icons.close_fullscreen_outlined),
                            titleTextStyle: const TextStyle(
                                color: CustomColors.mainPurple,
                                fontSize: 20,
                                fontWeight: FontWeight.bold),
                            descTextStyle: const TextStyle(
                                color: CustomColors.mainPurple,
                                fontSize: 17,
                                fontWeight: FontWeight.bold),
                            title: 'Внимание',
                            desc:
                                'Вы действительно хотите удалить трек номер ?',
                            btnCancelOnPress: () {},
                            btnOkOnPress: () async {
                              _deliveryInfoCubit.deleteDeliveryInfo(
                                trackId: widget.trackId,
                              );
                            },
                            buttonsTextStyle: const TextStyle(
                              color: CustomColors.mainPurple,
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                            ),
                            dialogBackgroundColor: CustomColors.mainColor,
                            btnOkText: 'Да',
                            btnOkColor: CustomColors.mainYellow,
                            btnCancelText: 'Нет',
                          ).show();
                        },
                        style: ElevatedButton.styleFrom(
                          elevation: 0.0,
                          backgroundColor: CustomColors.mainYellow,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                        ),
                        child: CustomText(
                          text: "Удалить трек номер",
                          textStyle: GoogleFonts.montserrat(
                            color: CustomColors.mainPurple,
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
