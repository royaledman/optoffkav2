part of 'banner_cubit.dart';

@freezed
class BannerState with _$BannerState {
  const factory BannerState.getBanner({
    required List<BannerModel> bannerModel,
  }) = _GetBanner;
  const factory BannerState.error() = _Error;
}
