import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:optoffka/feature/user/domain/entities/banner_model/banner_model.dart';
import 'package:optoffka/feature/user/domain/repository/user_repository.dart';

part 'banner_state.dart';
part 'banner_cubit.freezed.dart';

@singleton
class BannerCubit extends Cubit<BannerState> {
  final UserRepository _userRepository;
  BannerCubit(
    this._userRepository,
  ) : super(
          const BannerState.error(),
        );

  Future<void> getBanner() async {
    try {
      final bannerModel = await _userRepository.getBanner();
      emit(
        BannerState.getBanner(
          bannerModel: bannerModel,
        ),
      );
    } on DioException {
      emit(
        const BannerState.error(),
      );
    }
  }
}
