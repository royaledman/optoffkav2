// ignore_for_file: deprecated_member_use

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:optoffka/core/injection/injectable.dart';
import 'package:optoffka/core/navigation/app_router.dart';
import 'package:optoffka/feature/user/presentation/banner/cubit/banner_cubit.dart';
import 'package:optoffka/feature/user/presentation/comman/appbar.dart';
import 'package:optoffka/feature/user/presentation/comman/colors.dart';
import 'package:optoffka/feature/user/presentation/comman/text.dart';
import 'package:optoffka/feature/user/presentation/cubit/cubit/user_cubit.dart';
import 'package:url_launcher/url_launcher.dart';

@RoutePage()
class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final banner = getIt<BannerCubit>();

  @override
  void initState() {
    banner.getBanner();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<UserCubit, UserState>(
      bloc: getIt.get<UserCubit>(),
      listener: (context, state) {
        state.whenOrNull(unauthorized: () {
          context.router.popUntilRoot();
          context.router.replace(const AuthRoute());
        });
      },
      child: Scaffold(
        backgroundColor: CustomColors.mainColor,
        bottomNavigationBar: SizedBox(
          height: 70,
          width: 380,
          child: ElevatedButton.icon(
            icon: const Icon(
              Icons.add_rounded,
              color: CustomColors.mainPurple,
              size: 40,
            ),
            label: const Text(
              "Добавить трек номер",
              style: TextStyle(color: CustomColors.mainPurple, fontSize: 20),
            ),
            onPressed: () => context.router.push(const AddedTrackNumberRoute()),
            style: ElevatedButton.styleFrom(
              backgroundColor: CustomColors.mainYellow,
              shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30)),
              ),
            ),
          ),
        ),
        appBar: const OptoffkaAppbar(),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                margin: const EdgeInsets.only(left: 10),
                child: CustomText(
                  text: "Добро пожаловать",
                  textStyle: GoogleFonts.montserrat(
                      fontSize: 20,
                      height: 3,
                      fontWeight: FontWeight.w600,
                      color: CustomColors.mainPurple),
                ),
              ),
              Wrap(
                spacing: 20,
                runSpacing: 20,
                // mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  InkWell(
                    onTap: () => context.router.push(
                      const MyDeliveryRoute(),
                    ),
                    child: Container(
                      height: 140,
                      width: MediaQuery.of(context).size.width - 30,
                      decoration: BoxDecoration(
                        color: CustomColors.mainYellow,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      padding: const EdgeInsets.all(8),
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SvgPicture.asset(
                              'assets/images/box.svg',
                              height: 60,
                              width: 60,
                              color: CustomColors.mainPurple,
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            CustomText(
                              text: 'Мои посылки',
                              maxLines: 2,
                              textStyle: GoogleFonts.montserrat(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600,
                                  color: CustomColors.mainPurple),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () => context.router.push(const RuleRoute()),
                    child: Container(
                      height: 140,
                      width: MediaQuery.of(context).size.width / 2 - 30,
                      decoration: BoxDecoration(
                        color: CustomColors.mainYellow,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      padding: const EdgeInsets.all(8),
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SvgPicture.asset(
                              'assets/images/rule.svg',
                              height: 60,
                              width: 60,
                              color: CustomColors.mainPurple,
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            CustomText(
                              text: 'Правила\nсотрудничества',
                              maxLines: 2,
                              textStyle: GoogleFonts.montserrat(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 16,
                                  color: CustomColors.mainPurple),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  // InkWell(
                  //   onTap: () => context.router.push(
                  //     const SearchRoute(),
                  //   ),
                  //   child: Container(
                  //     height: 140,
                  //     width: MediaQuery.of(context).size.width / 2 - 30,
                  //     decoration: BoxDecoration(
                  //       color: CustomColors.mainYellow,
                  //       borderRadius: BorderRadius.circular(15),
                  //     ),
                  //     padding: const EdgeInsets.all(8),
                  //     child: Center(
                  //       child: Column(
                  //         mainAxisAlignment: MainAxisAlignment.center,
                  //         children: [
                  //           SvgPicture.asset(
                  //             'assets/images/search.svg',
                  //             height: 60,
                  //             width: 60,
                  //             color: CustomColors.mainPurple,
                  //           ),
                  //           const SizedBox(
                  //             height: 10,
                  //           ),
                  //           CustomText(
                  //             text: 'Поиск посылки',
                  //             maxLines: 2,
                  //             textStyle: GoogleFonts.montserrat(
                  //               fontSize: 16,
                  //               fontWeight: FontWeight.w600,
                  //               color: CustomColors.mainPurple,
                  //             ),
                  //           )
                  //         ],
                  //       ),
                  //     ),
                  //   ),
                  // ),

                  InkWell(
                    onTap: () => context.router.push(
                      const SecureRuleRoute(),
                    ),
                    child: Container(
                      height: 140,
                      width: MediaQuery.of(context).size.width / 2 - 30,
                      decoration: BoxDecoration(
                        color: CustomColors.mainYellow,
                        borderRadius: BorderRadius.circular(15),
                      ),
                      padding: const EdgeInsets.all(8),
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Icon(
                              Icons.assessment_outlined,
                              size: 60,
                              color: CustomColors.mainPurple,
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            CustomText(
                              text: 'Условия \n хранения',
                              maxLines: 2,
                              textStyle: GoogleFonts.montserrat(
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                                color: CustomColors.mainPurple,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                margin: const EdgeInsets.only(left: 10),
                child: CustomText(
                  text: "Последние новости",
                  textStyle: GoogleFonts.montserrat(
                      fontSize: 20,
                      height: 3,
                      fontWeight: FontWeight.w600,
                      color: CustomColors.mainPurple),
                ),
              ),
              BlocBuilder<BannerCubit, BannerState>(
                bloc: banner,
                builder: (context, state) {
                  return state.maybeWhen(
                    orElse: () =>
                        const Center(child: CircularProgressIndicator()),
                    getBanner: (bannerModel) => Container(
                      margin: const EdgeInsets.only(bottom: 20),
                      height: 220,
                      child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, index) => GestureDetector(
                          onTap: () => launch(bannerModel[index].link),
                          child: Container(
                            margin: const EdgeInsets.only(
                              left: 10,
                            ),
                            height: 120,
                            width: 150,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              image: DecorationImage(
                                image: NetworkImage(bannerModel[index].image),
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                        ),
                        itemCount: bannerModel.length,
                      ),
                    ),
                  );
                },
              ),
              Center(
                child: SizedBox(
                  height: 60,
                  width: 350,
                  child: ElevatedButton(
                    onPressed: () =>
                        context.router.push(const MyReceiptsRoute()),
                    style: ElevatedButton.styleFrom(
                      backgroundColor: CustomColors.mainYellow,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                    ),
                    child: CustomText(
                      text: 'Мои поступления',
                      textStyle: GoogleFonts.montserrat(
                          fontSize: 20,
                          fontWeight: FontWeight.w600,
                          color: CustomColors.mainPurple),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
