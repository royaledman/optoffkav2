import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:optoffka/feature/user/domain/entities/rule_model/rule_model.dart';
import 'package:optoffka/feature/user/domain/repository/user_repository.dart';

part 'rule_state.dart';
part 'rule_cubit.freezed.dart';

@injectable
class RuleCubit extends Cubit<RuleState> {
  final UserRepository _userRepository;
  RuleCubit(this._userRepository) : super(const RuleState.error());

  Future<void> getRule() async {
    try {
      final ruleModel = await _userRepository.getRule();
      emit(
        RuleState.loading(ruleModel: ruleModel),
      );
    } on DioException {
      emit(
        const RuleState.error(),
      );
    }
  }
}
