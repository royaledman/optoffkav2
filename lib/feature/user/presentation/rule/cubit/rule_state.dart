part of 'rule_cubit.dart';

@freezed
class RuleState with _$RuleState {
  const factory RuleState.loading({
    required List<RuleModel> ruleModel,
  }) = _Loading;
  const factory RuleState.error() = _Error;
}
