import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:optoffka/core/injection/injectable.dart';
import 'package:optoffka/feature/user/presentation/comman/appbar.dart';
import 'package:optoffka/feature/user/presentation/comman/colors.dart';
import 'package:optoffka/feature/user/presentation/comman/text.dart';
import 'package:optoffka/feature/user/presentation/rule/cubit/rule_cubit.dart';

@RoutePage()
class RulePage extends StatefulWidget {
  const RulePage({super.key});

  @override
  State<RulePage> createState() => _RulePageState();
}

class _RulePageState extends State<RulePage> {
  final ruleCubit = getIt<RuleCubit>();

  @override
  void initState() {
    ruleCubit.getRule();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.mainColor,
      appBar: const OptoffkaAppbar(),
      body: BlocBuilder<RuleCubit, RuleState>(
        bloc: ruleCubit,
        builder: (context, state) {
          return state.maybeWhen(
            orElse: () => const Center(
              child: SingleChildScrollView(),
            ),
            loading: (ruleModel) => SingleChildScrollView(
              child: ListView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (conCustomText, data) {
                  return Column(
                    children: <Widget>[
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Container(
                          margin: const EdgeInsets.only(left: 20),
                          child: CustomText(
                            text: "Условия и сроки доставки",
                            textStyle: GoogleFonts.montserrat(
                                color: CustomColors.mainPurple,
                                fontWeight: FontWeight.bold,
                                fontSize: 18),
                          ),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.all(10),
                        margin: const EdgeInsets.only(
                            top: 20, right: 15, bottom: 20, left: 15),
                        // height: 590,
                        width: 500,
                        decoration: BoxDecoration(
                          border: Border.all(
                              color: CustomColors.mainYellow, width: 1),
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: CustomText(
                          text: ruleModel[data].conditions,
                          textStyle: GoogleFonts.montserrat(fontSize: 15),
                        ),
                      ),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Container(
                          margin: const EdgeInsets.only(left: 20),
                          child: CustomText(
                            text: "Хрупкий товар",
                            textStyle: GoogleFonts.montserrat(
                                color: CustomColors.mainPurple,
                                fontWeight: FontWeight.bold,
                                fontSize: 18),
                          ),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.all(10),
                        margin: const EdgeInsets.only(
                            top: 20, right: 15, bottom: 20, left: 15),
                        width: 520,
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: CustomColors.mainYellow, width: 1),
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(15)),
                        child: CustomText(
                          text: ruleModel[data].shipment,
                          textStyle: GoogleFonts.montserrat(fontSize: 15),
                        ),
                      ),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Container(
                          margin: const EdgeInsets.only(left: 20),
                          child: CustomText(
                            text: "Отправка из Алматы по регионам",
                            textStyle: GoogleFonts.montserrat(
                                color: CustomColors.mainPurple,
                                fontWeight: FontWeight.bold,
                                fontSize: 18),
                          ),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.all(10),
                        margin: const EdgeInsets.only(
                            top: 20, right: 15, bottom: 20, left: 15),
                        width: 520,
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: CustomColors.mainYellow, width: 1),
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(15)),
                        child: CustomText(
                          text: ruleModel[data].regions,
                          textStyle: GoogleFonts.montserrat(fontSize: 15),
                        ),
                      ),
                    ],
                  );
                },
                itemCount: ruleModel.length,
              ),
            ),
          );
        },
      ),
    );
  }
}
