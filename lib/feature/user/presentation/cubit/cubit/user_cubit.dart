import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:optoffka/feature/user/domain/entities/login_dto/login_dto.dart';
import 'package:optoffka/feature/user/domain/entities/update_user_dto.dart/update_user_dto.dart';
import 'package:optoffka/feature/user/domain/entities/user_model/user_model.dart';
import 'package:optoffka/feature/user/domain/repository/user_repository.dart';

part 'user_state.dart';
part 'user_cubit.freezed.dart';

@singleton
class UserCubit extends Cubit<UserState> {
  final UserRepository _userRepository;
  UserCubit(
    this._userRepository,
  ) : super(const UserState.unauthorized());

  Future<void> logout() async {
    try {
      _userRepository.logout();

      emit(const UserState.unauthorized());
    } on DioException catch (e) {
      emit(UserState.onError(
        message: e.response?.statusMessage ?? '',
      ));
    }
  }

  Future<void> login({
    required String login,
    required String password,
  }) async {
    try {
      emit(const UserState.loading());
      await _userRepository.login(
          dto: LoginDto(
        deviceToken: 'deviceToken',
        login: login,
        password: password,
      ));

      final userModel = await _userRepository.getUser();

      emit(
        UserState.authorized(
          userModel: userModel,
        ),
      );
    } on DioException catch (e) {
      emit(UserState.onError(
        message: e.response?.statusMessage ?? '',
      ));
    }
  }

  Future<void> getUserToken() async {
    try {
      emit(
        UserState.authorized(
          userModel: await _userRepository.getUser(),
        ),
      );
    } on DioException {
      emit(const UserState.unauthorized());
    }
  }

  Future<void> updateProfile({
    required String name,
    required String password,
    required String city,
    required String phone,
  }) async {
    try {
      emit(const UserState.loading());
      await _userRepository.updateProfile(
        dto: UpdateUserDto(
          name: name,
          city: city,
          phone: phone,
          password: password,
        ),
      );
      emit(
        const UserState.updated(),
      );

      final userModel = await _userRepository.getUser();
      emit(
        UserState.authorized(
          userModel: userModel,
        ),
      );
    } on DioException catch (e) {
      emit(UserState.onError(
        message: e.response?.statusMessage ?? '',
      ));
    }
  }
}
