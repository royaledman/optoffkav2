part of 'user_cubit.dart';

@freezed
class UserState with _$UserState {
  const factory UserState.unauthorized() = _Unauthorized;
  const factory UserState.authorized({ 
    required  UserModel userModel, 
  }) = _Authorized;
  const factory UserState.updated() = _Updated;
  const factory UserState.loading() = _Loading;
  const factory UserState.onError({
    required String message,
  }) = _Error;
}
