import 'package:dio/dio.dart';
import 'package:optoffka/feature/user/domain/entities/added_tracknumber_dto/added_dto.dart';
import 'package:optoffka/feature/user/domain/entities/login_dto/login_dto.dart';
import 'package:optoffka/feature/user/domain/entities/update_user_dto.dart/update_user_dto.dart';

abstract class UserRemoteDataSource {
  static String auth = '/auth/login';
  static String updateUserApi = '/user/update';
  static String getUserApi = '/user/staticInfo';
  static String getBannerApi = '/auth/articles';
  static String createTrackApi = '/create/track';
  static String getNotificationBannerApi = '/user/notifications';
  static String getMyDeliveryApi = '/show/track';
  static String getMyDeliveryInfoApi = '/show/track/info';
  static String deleteApi = '/delete/track';
  static String ruleApi = '/info';
  static String receiptsApi = '/show/user/info';
  static String searchApi = '/show/stage';
  static String updateDescriptionApi = '/edit/track';
  static String historyApi = '/history/track/';


  Future<Response> login({
    required LoginDto loginDto,
  });

  Future<Response> updateUser({
    required UpdateUserDto updateUserDto,
  });

  Future<Response> createTrack({
    required AddedDto addedDto,
  });

  Future<Response> getUser();

  Future<Response> getBanner();

  Future<Response> getNotificationBanner();

  Future<Response> getMyDelivery({
    String? stageId,
    String? description,
  });

  Future<Response> getMyDeliveryInfo({required int trackId});

  Future<Response> deleteDelivery({required int trackId});

  Future<Response> getRule();

  Future<Response> getReceipts();
  
  Future<Response> historyTrack({
     required String ship,
  });

  Future<Response> searchTrackNumber({String? search});

  Future<void> editTrack({
    required int shipId,
    required String description,
  });
}
