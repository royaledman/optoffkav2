import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:optoffka/core/network/dio.dart';
import 'package:optoffka/feature/user/data/data_source/remote/user_remote_data_source.dart';
import 'package:optoffka/feature/user/domain/entities/added_tracknumber_dto/added_dto.dart';
import 'package:optoffka/feature/user/domain/entities/login_dto/login_dto.dart';
import 'package:optoffka/feature/user/domain/entities/update_user_dto.dart/update_user_dto.dart';

@Injectable(as: UserRemoteDataSource)
class UserRemoteDataSourceImpl implements UserRemoteDataSource {
  final DioClient dioClient;

  UserRemoteDataSourceImpl({
    required this.dioClient,
  });

  @override
  Future<Response> login({
    required LoginDto loginDto,
  }) {
    return dioClient.instance.post(
      UserRemoteDataSource.auth,
      data: loginDto.toJson(),
    );
  }

  @override
  Future<Response> getUser() {
    return dioClient.instance.get(UserRemoteDataSource.getUserApi);
  }

  @override
  Future<Response> getBanner() {
    return dioClient.instance.get(UserRemoteDataSource.getBannerApi);
  }

  @override
  Future<Response> updateUser({
    required UpdateUserDto updateUserDto,
  }) {
    return dioClient.instance.post(
      UserRemoteDataSource.updateUserApi,
      data: updateUserDto.toJson(),
    );
  }

  @override
  Future<Response> createTrack({
    required AddedDto addedDto,
  }) {
    return dioClient.instance.post(
      UserRemoteDataSource.createTrackApi,
      data: addedDto.toJson(),
    );
  }

  @override
  Future<Response> getNotificationBanner() {
    return dioClient.instance
        .get(UserRemoteDataSource.getNotificationBannerApi);
  }

  @override
  Future<Response> getMyDelivery({
    String? stageId,
    String? description,
  }) {
    return dioClient.instance.get(
      '${UserRemoteDataSource.getMyDeliveryApi}${stageId != null ? '/$stageId' : ''}',
      queryParameters: {
        "description" : description,
      }, 
    );
  }

  @override
  Future<Response> getMyDeliveryInfo({required int trackId}) {
    return dioClient.instance
        .get("${UserRemoteDataSource.getMyDeliveryInfoApi}/$trackId");
  }

  @override
  Future<Response> deleteDelivery({required int trackId}) {
    return dioClient.instance.get("${UserRemoteDataSource.deleteApi}/$trackId");
  }

  @override
  Future<Response> getRule() {
    return dioClient.instance.get(UserRemoteDataSource.ruleApi);
  }

  @override
  Future<Response> getReceipts() {
    return dioClient.instance.get(UserRemoteDataSource.receiptsApi);
  }

  @override
  Future<Response> searchTrackNumber({String? search}) {
    return dioClient.instance.get(
      UserRemoteDataSource.searchApi,
      queryParameters: {
        "track_number": search ?? '',
      },
    );
  }

  @override
  Future<void> editTrack({
    required int shipId,
    required String description,
  }) async {
    dioClient.instance
        .post('${UserRemoteDataSource.updateDescriptionApi}/$shipId', data: {
      'description': description,
    });
  }

  @override
  Future<Response> historyTrack({
    required String ship,
  }) async {
   return dioClient.instance.get('${UserRemoteDataSource.historyApi}/$ship/user');
  }
}
