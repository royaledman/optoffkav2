import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:injectable/injectable.dart';
import 'package:optoffka/feature/user/data/data_source/local/user_local_data_source.dart';

@Injectable(as: UserLocalDataSource)
class UserLocalDataSourceImpl implements UserLocalDataSource {
  final FlutterSecureStorage flutterSecureStorage;

  UserLocalDataSourceImpl({
    required this.flutterSecureStorage,
  });

  @override
  Future<void> saveUser({
    required int user,
  }) =>
      flutterSecureStorage.write(
        key: UserLocalDataSource.user,
        value: user.toString(),
      );

  @override
  Future<String?> getToken() async {
    return flutterSecureStorage.read(
      key: UserLocalDataSource.token,
    );
  }

  @override 
  Future<void> logout() =>
      flutterSecureStorage.delete(key: UserLocalDataSource.token);

  @override
  Future<String?> getDeviceToken() => flutterSecureStorage.read(
        key: UserLocalDataSource.deviceToken,
      );
}
