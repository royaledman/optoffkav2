
abstract class UserLocalDataSource {

  static String token = 'token';
  static String user = 'user';
  static String deviceToken = 'deviceToken';

  Future<void> logout();

  Future<void> saveUser({
    required int user,
  });

  Future<String?> getToken();

  Future<String?> getDeviceToken();
}
