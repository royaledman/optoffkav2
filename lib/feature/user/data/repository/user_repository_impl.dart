import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:optoffka/feature/user/data/data_source/local/user_local_data_source.dart';
import 'package:optoffka/feature/user/data/data_source/remote/user_remote_data_source.dart';
import 'package:optoffka/feature/user/domain/entities/added_tracknumber_dto/added_dto.dart';
import 'package:optoffka/feature/user/domain/entities/history_model/history_model.dart';
import 'package:optoffka/feature/user/domain/entities/login_dto/login_dto.dart';
import 'package:optoffka/feature/user/domain/entities/my_delivery_info_model/my_delivery_info_model.dart';
import 'package:optoffka/feature/user/domain/entities/my_delivery_model/my_delivery_model.dart';
import 'package:optoffka/feature/user/domain/entities/notification_model/notification_model.dart';
import 'package:optoffka/feature/user/domain/entities/receipts_model/receipts_model.dart';
import 'package:optoffka/feature/user/domain/entities/rule_model/rule_model.dart';
import 'package:optoffka/feature/user/domain/entities/update_user_dto.dart/update_user_dto.dart';
import 'package:optoffka/feature/user/domain/entities/user_model/user_model.dart';
import 'package:optoffka/feature/user/domain/repository/user_repository.dart';
import 'package:optoffka/feature/user/domain/entities/banner_model/banner_model.dart';

@Injectable(as: UserRepository)
class UserRepositoryImpl implements UserRepository {
  final UserLocalDataSource localDataSource;
  final UserRemoteDataSource userRemoteDataSource;

  UserRepositoryImpl(
    this.userRemoteDataSource,
    this.localDataSource,
  );

  @override
  Future<int> login({
    required LoginDto dto,
  }) async {
    final deviceToken = await localDataSource.getDeviceToken();

    final response = await userRemoteDataSource.login(
      loginDto: dto.copyWith(
        deviceToken: deviceToken ?? '',
      ),
    );

    if (response.data['user'] != null) {
      await localDataSource.saveUser(user: response.data['user']);

      return response.data['user'];
    } else {
      throw DioException(
        requestOptions: RequestOptions(),
        response: Response(
          requestOptions: RequestOptions(),
          statusMessage: response.data['message'],
        ),
      );
    }
  }

  @override
  Future<UserModel> getUser() async {
    final response = await userRemoteDataSource.getUser();
    return UserModel.fromJson(response.data['data']);
  }

  @override
  Future<List<BannerModel>> getBanner() async {
    final response = await userRemoteDataSource.getBanner();
    return response.data['data']
        .map<BannerModel>((e) => BannerModel.fromJson(e))
        .toList();
  }

  @override
  Future<void> updateProfile({required UpdateUserDto dto}) =>
      userRemoteDataSource.updateUser(
        updateUserDto: dto,
      );

  @override
  Future<void> createTrack({required AddedDto dto}) =>
      userRemoteDataSource.createTrack(
        addedDto: dto,
      );

  @override
  Future<List<NotificationModel>> getNotificationBanner() async {
    final response = await userRemoteDataSource.getNotificationBanner();
    return response.data['data']
        .map<NotificationModel>((e) => NotificationModel.fromJson(e))
        .toList();
  }

  @override
  Future<List<MyDeliveryModel>> getMyDelivery({
    String? stageId,
    String? description,
  }) async {
    final response = await userRemoteDataSource.getMyDelivery(
      stageId: stageId,
      description: description,
    );
    return response.data['data']
        .map<MyDeliveryModel>((e) => MyDeliveryModel.fromJson(e))
        .toList();
  }

  @override
  Future<void> deleteDelivery({required int trackId}) =>
      userRemoteDataSource.deleteDelivery(
        trackId: trackId,
      );

  @override
  Future<MyDeliveryInfoModel> getMyDeliveryInfo({required int trackId}) async {
    final response = await userRemoteDataSource.getMyDeliveryInfo(
      trackId: trackId,
    );

    return MyDeliveryInfoModel.fromJson(response.data['data']);
  }

  @override
  Future<List<RuleModel>> getRule() async {
    final response = await userRemoteDataSource.getRule();
    return response.data['data']
        .map<RuleModel>((e) => RuleModel.fromJson(e))
        .toList();
  }

  @override
  Future<List<ReceiptsModel>> receiptsModel() async {
    final response = await userRemoteDataSource.getReceipts();
    return response.data['data']
        .map<ReceiptsModel>((e) => ReceiptsModel.fromJson(e))
        .toList();
  }

  @override
  Future<List<MyDeliveryModel>> searchTrackNumber({String? search}) async {
    final response =
        await userRemoteDataSource.searchTrackNumber(search: search);

    return response.data['data']
        .map<MyDeliveryModel>((e) => MyDeliveryModel.fromJson(e))
        .toList();
  }

  @override
  Future<void> updateShipDescription(
          {required int shipId, required String description}) =>
      userRemoteDataSource.editTrack(
        shipId: shipId,
        description: description,
      );

  @override
  Future<void> logout() => localDataSource.logout();

  @override
  Future<HistoryModel> getHistory({
    required String ship,
  }) async {
    final response = await userRemoteDataSource.historyTrack(ship: ship);
    return HistoryModel.fromJson(response.data['data']);
  }
}
