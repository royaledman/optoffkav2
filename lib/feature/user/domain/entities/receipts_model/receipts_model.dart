// ignore_for_file: invalid_annotation_target

import 'package:freezed_annotation/freezed_annotation.dart';

part 'receipts_model.g.dart';
part 'receipts_model.freezed.dart';

@freezed
class ReceiptsModel with _$ReceiptsModel {
  const factory ReceiptsModel({
    @JsonKey(name: 'parish_sum') required String parishSum,
    required String weight,
    required String date,
    required String quantity,
  }) = _ReceiptsModel;

  factory ReceiptsModel.fromJson(Map<String, dynamic> json) =>
      _$ReceiptsModelFromJson(json);
}
