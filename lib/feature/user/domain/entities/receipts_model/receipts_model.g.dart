// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'receipts_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$ReceiptsModelImpl _$$ReceiptsModelImplFromJson(Map<String, dynamic> json) =>
    _$ReceiptsModelImpl(
      parishSum: json['parish_sum'] as String,
      weight: json['weight'] as String,
      date: json['date'] as String,
      quantity: json['quantity'] as String,
    );

Map<String, dynamic> _$$ReceiptsModelImplToJson(_$ReceiptsModelImpl instance) =>
    <String, dynamic>{
      'parish_sum': instance.parishSum,
      'weight': instance.weight,
      'date': instance.date,
      'quantity': instance.quantity,
    };
