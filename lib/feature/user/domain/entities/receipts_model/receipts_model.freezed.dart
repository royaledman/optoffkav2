// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'receipts_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ReceiptsModel _$ReceiptsModelFromJson(Map<String, dynamic> json) {
  return _ReceiptsModel.fromJson(json);
}

/// @nodoc
mixin _$ReceiptsModel {
  @JsonKey(name: 'parish_sum')
  String get parishSum => throw _privateConstructorUsedError;
  String get weight => throw _privateConstructorUsedError;
  String get date => throw _privateConstructorUsedError;
  String get quantity => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ReceiptsModelCopyWith<ReceiptsModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ReceiptsModelCopyWith<$Res> {
  factory $ReceiptsModelCopyWith(
          ReceiptsModel value, $Res Function(ReceiptsModel) then) =
      _$ReceiptsModelCopyWithImpl<$Res, ReceiptsModel>;
  @useResult
  $Res call(
      {@JsonKey(name: 'parish_sum') String parishSum,
      String weight,
      String date,
      String quantity});
}

/// @nodoc
class _$ReceiptsModelCopyWithImpl<$Res, $Val extends ReceiptsModel>
    implements $ReceiptsModelCopyWith<$Res> {
  _$ReceiptsModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? parishSum = null,
    Object? weight = null,
    Object? date = null,
    Object? quantity = null,
  }) {
    return _then(_value.copyWith(
      parishSum: null == parishSum
          ? _value.parishSum
          : parishSum // ignore: cast_nullable_to_non_nullable
              as String,
      weight: null == weight
          ? _value.weight
          : weight // ignore: cast_nullable_to_non_nullable
              as String,
      date: null == date
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as String,
      quantity: null == quantity
          ? _value.quantity
          : quantity // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ReceiptsModelImplCopyWith<$Res>
    implements $ReceiptsModelCopyWith<$Res> {
  factory _$$ReceiptsModelImplCopyWith(
          _$ReceiptsModelImpl value, $Res Function(_$ReceiptsModelImpl) then) =
      __$$ReceiptsModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: 'parish_sum') String parishSum,
      String weight,
      String date,
      String quantity});
}

/// @nodoc
class __$$ReceiptsModelImplCopyWithImpl<$Res>
    extends _$ReceiptsModelCopyWithImpl<$Res, _$ReceiptsModelImpl>
    implements _$$ReceiptsModelImplCopyWith<$Res> {
  __$$ReceiptsModelImplCopyWithImpl(
      _$ReceiptsModelImpl _value, $Res Function(_$ReceiptsModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? parishSum = null,
    Object? weight = null,
    Object? date = null,
    Object? quantity = null,
  }) {
    return _then(_$ReceiptsModelImpl(
      parishSum: null == parishSum
          ? _value.parishSum
          : parishSum // ignore: cast_nullable_to_non_nullable
              as String,
      weight: null == weight
          ? _value.weight
          : weight // ignore: cast_nullable_to_non_nullable
              as String,
      date: null == date
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as String,
      quantity: null == quantity
          ? _value.quantity
          : quantity // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$ReceiptsModelImpl implements _ReceiptsModel {
  const _$ReceiptsModelImpl(
      {@JsonKey(name: 'parish_sum') required this.parishSum,
      required this.weight,
      required this.date,
      required this.quantity});

  factory _$ReceiptsModelImpl.fromJson(Map<String, dynamic> json) =>
      _$$ReceiptsModelImplFromJson(json);

  @override
  @JsonKey(name: 'parish_sum')
  final String parishSum;
  @override
  final String weight;
  @override
  final String date;
  @override
  final String quantity;

  @override
  String toString() {
    return 'ReceiptsModel(parishSum: $parishSum, weight: $weight, date: $date, quantity: $quantity)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ReceiptsModelImpl &&
            (identical(other.parishSum, parishSum) ||
                other.parishSum == parishSum) &&
            (identical(other.weight, weight) || other.weight == weight) &&
            (identical(other.date, date) || other.date == date) &&
            (identical(other.quantity, quantity) ||
                other.quantity == quantity));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, parishSum, weight, date, quantity);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ReceiptsModelImplCopyWith<_$ReceiptsModelImpl> get copyWith =>
      __$$ReceiptsModelImplCopyWithImpl<_$ReceiptsModelImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$ReceiptsModelImplToJson(
      this,
    );
  }
}

abstract class _ReceiptsModel implements ReceiptsModel {
  const factory _ReceiptsModel(
      {@JsonKey(name: 'parish_sum') required final String parishSum,
      required final String weight,
      required final String date,
      required final String quantity}) = _$ReceiptsModelImpl;

  factory _ReceiptsModel.fromJson(Map<String, dynamic> json) =
      _$ReceiptsModelImpl.fromJson;

  @override
  @JsonKey(name: 'parish_sum')
  String get parishSum;
  @override
  String get weight;
  @override
  String get date;
  @override
  String get quantity;
  @override
  @JsonKey(ignore: true)
  _$$ReceiptsModelImplCopyWith<_$ReceiptsModelImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
