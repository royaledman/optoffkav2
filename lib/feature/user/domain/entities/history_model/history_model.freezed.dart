// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'history_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

HistoryModel _$HistoryModelFromJson(Map<String, dynamic> json) {
  return _HistoryModel.fromJson(json);
}

/// @nodoc
mixin _$HistoryModel {
  DateTime? get created_at => throw _privateConstructorUsedError;
  DateTime? get second_stage_date => throw _privateConstructorUsedError;
  DateTime? get third_stage_date => throw _privateConstructorUsedError;
  DateTime? get fourth_stage_date => throw _privateConstructorUsedError;
  DateTime? get fifth_stage_date => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $HistoryModelCopyWith<HistoryModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HistoryModelCopyWith<$Res> {
  factory $HistoryModelCopyWith(
          HistoryModel value, $Res Function(HistoryModel) then) =
      _$HistoryModelCopyWithImpl<$Res, HistoryModel>;
  @useResult
  $Res call(
      {DateTime? created_at,
      DateTime? second_stage_date,
      DateTime? third_stage_date,
      DateTime? fourth_stage_date,
      DateTime? fifth_stage_date});
}

/// @nodoc
class _$HistoryModelCopyWithImpl<$Res, $Val extends HistoryModel>
    implements $HistoryModelCopyWith<$Res> {
  _$HistoryModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? created_at = freezed,
    Object? second_stage_date = freezed,
    Object? third_stage_date = freezed,
    Object? fourth_stage_date = freezed,
    Object? fifth_stage_date = freezed,
  }) {
    return _then(_value.copyWith(
      created_at: freezed == created_at
          ? _value.created_at
          : created_at // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      second_stage_date: freezed == second_stage_date
          ? _value.second_stage_date
          : second_stage_date // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      third_stage_date: freezed == third_stage_date
          ? _value.third_stage_date
          : third_stage_date // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      fourth_stage_date: freezed == fourth_stage_date
          ? _value.fourth_stage_date
          : fourth_stage_date // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      fifth_stage_date: freezed == fifth_stage_date
          ? _value.fifth_stage_date
          : fifth_stage_date // ignore: cast_nullable_to_non_nullable
              as DateTime?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$HistoryModelImplCopyWith<$Res>
    implements $HistoryModelCopyWith<$Res> {
  factory _$$HistoryModelImplCopyWith(
          _$HistoryModelImpl value, $Res Function(_$HistoryModelImpl) then) =
      __$$HistoryModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {DateTime? created_at,
      DateTime? second_stage_date,
      DateTime? third_stage_date,
      DateTime? fourth_stage_date,
      DateTime? fifth_stage_date});
}

/// @nodoc
class __$$HistoryModelImplCopyWithImpl<$Res>
    extends _$HistoryModelCopyWithImpl<$Res, _$HistoryModelImpl>
    implements _$$HistoryModelImplCopyWith<$Res> {
  __$$HistoryModelImplCopyWithImpl(
      _$HistoryModelImpl _value, $Res Function(_$HistoryModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? created_at = freezed,
    Object? second_stage_date = freezed,
    Object? third_stage_date = freezed,
    Object? fourth_stage_date = freezed,
    Object? fifth_stage_date = freezed,
  }) {
    return _then(_$HistoryModelImpl(
      created_at: freezed == created_at
          ? _value.created_at
          : created_at // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      second_stage_date: freezed == second_stage_date
          ? _value.second_stage_date
          : second_stage_date // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      third_stage_date: freezed == third_stage_date
          ? _value.third_stage_date
          : third_stage_date // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      fourth_stage_date: freezed == fourth_stage_date
          ? _value.fourth_stage_date
          : fourth_stage_date // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      fifth_stage_date: freezed == fifth_stage_date
          ? _value.fifth_stage_date
          : fifth_stage_date // ignore: cast_nullable_to_non_nullable
              as DateTime?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$HistoryModelImpl implements _HistoryModel {
  const _$HistoryModelImpl(
      {this.created_at,
      this.second_stage_date,
      this.third_stage_date,
      this.fourth_stage_date,
      this.fifth_stage_date});

  factory _$HistoryModelImpl.fromJson(Map<String, dynamic> json) =>
      _$$HistoryModelImplFromJson(json);

  @override
  final DateTime? created_at;
  @override
  final DateTime? second_stage_date;
  @override
  final DateTime? third_stage_date;
  @override
  final DateTime? fourth_stage_date;
  @override
  final DateTime? fifth_stage_date;

  @override
  String toString() {
    return 'HistoryModel(created_at: $created_at, second_stage_date: $second_stage_date, third_stage_date: $third_stage_date, fourth_stage_date: $fourth_stage_date, fifth_stage_date: $fifth_stage_date)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$HistoryModelImpl &&
            (identical(other.created_at, created_at) ||
                other.created_at == created_at) &&
            (identical(other.second_stage_date, second_stage_date) ||
                other.second_stage_date == second_stage_date) &&
            (identical(other.third_stage_date, third_stage_date) ||
                other.third_stage_date == third_stage_date) &&
            (identical(other.fourth_stage_date, fourth_stage_date) ||
                other.fourth_stage_date == fourth_stage_date) &&
            (identical(other.fifth_stage_date, fifth_stage_date) ||
                other.fifth_stage_date == fifth_stage_date));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, created_at, second_stage_date,
      third_stage_date, fourth_stage_date, fifth_stage_date);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$HistoryModelImplCopyWith<_$HistoryModelImpl> get copyWith =>
      __$$HistoryModelImplCopyWithImpl<_$HistoryModelImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$HistoryModelImplToJson(
      this,
    );
  }
}

abstract class _HistoryModel implements HistoryModel {
  const factory _HistoryModel(
      {final DateTime? created_at,
      final DateTime? second_stage_date,
      final DateTime? third_stage_date,
      final DateTime? fourth_stage_date,
      final DateTime? fifth_stage_date}) = _$HistoryModelImpl;

  factory _HistoryModel.fromJson(Map<String, dynamic> json) =
      _$HistoryModelImpl.fromJson;

  @override
  DateTime? get created_at;
  @override
  DateTime? get second_stage_date;
  @override
  DateTime? get third_stage_date;
  @override
  DateTime? get fourth_stage_date;
  @override
  DateTime? get fifth_stage_date;
  @override
  @JsonKey(ignore: true)
  _$$HistoryModelImplCopyWith<_$HistoryModelImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
