// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'history_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$HistoryModelImpl _$$HistoryModelImplFromJson(Map<String, dynamic> json) =>
    _$HistoryModelImpl(
      created_at: json['created_at'] == null
          ? null
          : DateTime.parse(json['created_at'] as String),
      second_stage_date: json['second_stage_date'] == null
          ? null
          : DateTime.parse(json['second_stage_date'] as String),
      third_stage_date: json['third_stage_date'] == null
          ? null
          : DateTime.parse(json['third_stage_date'] as String),
      fourth_stage_date: json['fourth_stage_date'] == null
          ? null
          : DateTime.parse(json['fourth_stage_date'] as String),
      fifth_stage_date: json['fifth_stage_date'] == null
          ? null
          : DateTime.parse(json['fifth_stage_date'] as String),
    );

Map<String, dynamic> _$$HistoryModelImplToJson(_$HistoryModelImpl instance) =>
    <String, dynamic>{
      'created_at': instance.created_at?.toIso8601String(),
      'second_stage_date': instance.second_stage_date?.toIso8601String(),
      'third_stage_date': instance.third_stage_date?.toIso8601String(),
      'fourth_stage_date': instance.fourth_stage_date?.toIso8601String(),
      'fifth_stage_date': instance.fifth_stage_date?.toIso8601String(),
    };
