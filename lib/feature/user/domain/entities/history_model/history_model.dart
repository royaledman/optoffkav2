// ignore_for_file: non_constant_identifier_names

import 'package:freezed_annotation/freezed_annotation.dart';

part 'history_model.freezed.dart';
part 'history_model.g.dart';

@freezed
class HistoryModel with _$HistoryModel {
  const factory HistoryModel({
    DateTime? created_at,
    DateTime? second_stage_date,
    DateTime? third_stage_date,
    DateTime? fourth_stage_date,
    DateTime? fifth_stage_date,
  }) = _HistoryModel;
  factory HistoryModel.fromJson(Map<String, dynamic> json) =>
      _$HistoryModelFromJson(json);
}
