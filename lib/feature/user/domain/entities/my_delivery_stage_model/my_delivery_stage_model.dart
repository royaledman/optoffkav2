import 'package:freezed_annotation/freezed_annotation.dart';

part 'my_delivery_stage_model.freezed.dart';
part 'my_delivery_stage_model.g.dart';

@freezed
class MyDeliveryStageModel with _$MyDeliveryStageModel {
  const factory MyDeliveryStageModel({
    String? stage,
    required String name,
    String? description,
    int? id,
  }) = _MyDeliveryStageModel;
  factory MyDeliveryStageModel.fromJson(Map<String, dynamic> json) =>
      _$MyDeliveryStageModelFromJson(json);
}
