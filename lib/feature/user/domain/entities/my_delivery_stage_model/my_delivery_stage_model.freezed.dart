// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'my_delivery_stage_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

MyDeliveryStageModel _$MyDeliveryStageModelFromJson(Map<String, dynamic> json) {
  return _MyDeliveryStageModel.fromJson(json);
}

/// @nodoc
mixin _$MyDeliveryStageModel {
  String? get stage => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  String? get description => throw _privateConstructorUsedError;
  int? get id => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $MyDeliveryStageModelCopyWith<MyDeliveryStageModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MyDeliveryStageModelCopyWith<$Res> {
  factory $MyDeliveryStageModelCopyWith(MyDeliveryStageModel value,
          $Res Function(MyDeliveryStageModel) then) =
      _$MyDeliveryStageModelCopyWithImpl<$Res, MyDeliveryStageModel>;
  @useResult
  $Res call({String? stage, String name, String? description, int? id});
}

/// @nodoc
class _$MyDeliveryStageModelCopyWithImpl<$Res,
        $Val extends MyDeliveryStageModel>
    implements $MyDeliveryStageModelCopyWith<$Res> {
  _$MyDeliveryStageModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? stage = freezed,
    Object? name = null,
    Object? description = freezed,
    Object? id = freezed,
  }) {
    return _then(_value.copyWith(
      stage: freezed == stage
          ? _value.stage
          : stage // ignore: cast_nullable_to_non_nullable
              as String?,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      description: freezed == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$MyDeliveryStageModelImplCopyWith<$Res>
    implements $MyDeliveryStageModelCopyWith<$Res> {
  factory _$$MyDeliveryStageModelImplCopyWith(_$MyDeliveryStageModelImpl value,
          $Res Function(_$MyDeliveryStageModelImpl) then) =
      __$$MyDeliveryStageModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String? stage, String name, String? description, int? id});
}

/// @nodoc
class __$$MyDeliveryStageModelImplCopyWithImpl<$Res>
    extends _$MyDeliveryStageModelCopyWithImpl<$Res, _$MyDeliveryStageModelImpl>
    implements _$$MyDeliveryStageModelImplCopyWith<$Res> {
  __$$MyDeliveryStageModelImplCopyWithImpl(_$MyDeliveryStageModelImpl _value,
      $Res Function(_$MyDeliveryStageModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? stage = freezed,
    Object? name = null,
    Object? description = freezed,
    Object? id = freezed,
  }) {
    return _then(_$MyDeliveryStageModelImpl(
      stage: freezed == stage
          ? _value.stage
          : stage // ignore: cast_nullable_to_non_nullable
              as String?,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      description: freezed == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$MyDeliveryStageModelImpl implements _MyDeliveryStageModel {
  const _$MyDeliveryStageModelImpl(
      {this.stage, required this.name, this.description, this.id});

  factory _$MyDeliveryStageModelImpl.fromJson(Map<String, dynamic> json) =>
      _$$MyDeliveryStageModelImplFromJson(json);

  @override
  final String? stage;
  @override
  final String name;
  @override
  final String? description;
  @override
  final int? id;

  @override
  String toString() {
    return 'MyDeliveryStageModel(stage: $stage, name: $name, description: $description, id: $id)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$MyDeliveryStageModelImpl &&
            (identical(other.stage, stage) || other.stage == stage) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.description, description) ||
                other.description == description) &&
            (identical(other.id, id) || other.id == id));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, stage, name, description, id);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$MyDeliveryStageModelImplCopyWith<_$MyDeliveryStageModelImpl>
      get copyWith =>
          __$$MyDeliveryStageModelImplCopyWithImpl<_$MyDeliveryStageModelImpl>(
              this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$MyDeliveryStageModelImplToJson(
      this,
    );
  }
}

abstract class _MyDeliveryStageModel implements MyDeliveryStageModel {
  const factory _MyDeliveryStageModel(
      {final String? stage,
      required final String name,
      final String? description,
      final int? id}) = _$MyDeliveryStageModelImpl;

  factory _MyDeliveryStageModel.fromJson(Map<String, dynamic> json) =
      _$MyDeliveryStageModelImpl.fromJson;

  @override
  String? get stage;
  @override
  String get name;
  @override
  String? get description;
  @override
  int? get id;
  @override
  @JsonKey(ignore: true)
  _$$MyDeliveryStageModelImplCopyWith<_$MyDeliveryStageModelImpl>
      get copyWith => throw _privateConstructorUsedError;
}
