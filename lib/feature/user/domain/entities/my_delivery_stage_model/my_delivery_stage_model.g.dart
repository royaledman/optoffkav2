// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'my_delivery_stage_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$MyDeliveryStageModelImpl _$$MyDeliveryStageModelImplFromJson(
        Map<String, dynamic> json) =>
    _$MyDeliveryStageModelImpl(
      stage: json['stage'] as String?,
      name: json['name'] as String,
      description: json['description'] as String?,
      id: json['id'] as int?,
    );

Map<String, dynamic> _$$MyDeliveryStageModelImplToJson(
        _$MyDeliveryStageModelImpl instance) =>
    <String, dynamic>{
      'stage': instance.stage,
      'name': instance.name,
      'description': instance.description,
      'id': instance.id,
    };
