// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'my_delivery_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$MyDeliveryModelImpl _$$MyDeliveryModelImplFromJson(
        Map<String, dynamic> json) =>
    _$MyDeliveryModelImpl(
      id: json['id'] as int,
      trackNumber: json['track_number'] as String,
      description: json['description'] as String?,
      updatedAt: json['updated_at'] as String?,
      createdAt: json['created_at'] as String?,
      stage: json['stage'] == null
          ? null
          : MyDeliveryStageModel.fromJson(
              json['stage'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$MyDeliveryModelImplToJson(
        _$MyDeliveryModelImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'track_number': instance.trackNumber,
      'description': instance.description,
      'updated_at': instance.updatedAt,
      'created_at': instance.createdAt,
      'stage': instance.stage,
    };
