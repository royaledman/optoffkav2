// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'my_delivery_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

MyDeliveryModel _$MyDeliveryModelFromJson(Map<String, dynamic> json) {
  return _MyDeliveryModel.fromJson(json);
}

/// @nodoc
mixin _$MyDeliveryModel {
  int get id => throw _privateConstructorUsedError;
  @JsonKey(name: 'track_number')
  String get trackNumber => throw _privateConstructorUsedError;
  @JsonKey(name: 'description')
  String? get description => throw _privateConstructorUsedError;
  @JsonKey(name: 'updated_at')
  String? get updatedAt => throw _privateConstructorUsedError;
  @JsonKey(name: 'created_at')
  String? get createdAt => throw _privateConstructorUsedError;
  MyDeliveryStageModel? get stage => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $MyDeliveryModelCopyWith<MyDeliveryModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MyDeliveryModelCopyWith<$Res> {
  factory $MyDeliveryModelCopyWith(
          MyDeliveryModel value, $Res Function(MyDeliveryModel) then) =
      _$MyDeliveryModelCopyWithImpl<$Res, MyDeliveryModel>;
  @useResult
  $Res call(
      {int id,
      @JsonKey(name: 'track_number') String trackNumber,
      @JsonKey(name: 'description') String? description,
      @JsonKey(name: 'updated_at') String? updatedAt,
      @JsonKey(name: 'created_at') String? createdAt,
      MyDeliveryStageModel? stage});

  $MyDeliveryStageModelCopyWith<$Res>? get stage;
}

/// @nodoc
class _$MyDeliveryModelCopyWithImpl<$Res, $Val extends MyDeliveryModel>
    implements $MyDeliveryModelCopyWith<$Res> {
  _$MyDeliveryModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? trackNumber = null,
    Object? description = freezed,
    Object? updatedAt = freezed,
    Object? createdAt = freezed,
    Object? stage = freezed,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      trackNumber: null == trackNumber
          ? _value.trackNumber
          : trackNumber // ignore: cast_nullable_to_non_nullable
              as String,
      description: freezed == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      updatedAt: freezed == updatedAt
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as String?,
      createdAt: freezed == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String?,
      stage: freezed == stage
          ? _value.stage
          : stage // ignore: cast_nullable_to_non_nullable
              as MyDeliveryStageModel?,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $MyDeliveryStageModelCopyWith<$Res>? get stage {
    if (_value.stage == null) {
      return null;
    }

    return $MyDeliveryStageModelCopyWith<$Res>(_value.stage!, (value) {
      return _then(_value.copyWith(stage: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$MyDeliveryModelImplCopyWith<$Res>
    implements $MyDeliveryModelCopyWith<$Res> {
  factory _$$MyDeliveryModelImplCopyWith(_$MyDeliveryModelImpl value,
          $Res Function(_$MyDeliveryModelImpl) then) =
      __$$MyDeliveryModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int id,
      @JsonKey(name: 'track_number') String trackNumber,
      @JsonKey(name: 'description') String? description,
      @JsonKey(name: 'updated_at') String? updatedAt,
      @JsonKey(name: 'created_at') String? createdAt,
      MyDeliveryStageModel? stage});

  @override
  $MyDeliveryStageModelCopyWith<$Res>? get stage;
}

/// @nodoc
class __$$MyDeliveryModelImplCopyWithImpl<$Res>
    extends _$MyDeliveryModelCopyWithImpl<$Res, _$MyDeliveryModelImpl>
    implements _$$MyDeliveryModelImplCopyWith<$Res> {
  __$$MyDeliveryModelImplCopyWithImpl(
      _$MyDeliveryModelImpl _value, $Res Function(_$MyDeliveryModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? trackNumber = null,
    Object? description = freezed,
    Object? updatedAt = freezed,
    Object? createdAt = freezed,
    Object? stage = freezed,
  }) {
    return _then(_$MyDeliveryModelImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      trackNumber: null == trackNumber
          ? _value.trackNumber
          : trackNumber // ignore: cast_nullable_to_non_nullable
              as String,
      description: freezed == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      updatedAt: freezed == updatedAt
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as String?,
      createdAt: freezed == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String?,
      stage: freezed == stage
          ? _value.stage
          : stage // ignore: cast_nullable_to_non_nullable
              as MyDeliveryStageModel?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$MyDeliveryModelImpl implements _MyDeliveryModel {
  const _$MyDeliveryModelImpl(
      {required this.id,
      @JsonKey(name: 'track_number') required this.trackNumber,
      @JsonKey(name: 'description') this.description,
      @JsonKey(name: 'updated_at') this.updatedAt,
      @JsonKey(name: 'created_at') this.createdAt,
      this.stage});

  factory _$MyDeliveryModelImpl.fromJson(Map<String, dynamic> json) =>
      _$$MyDeliveryModelImplFromJson(json);

  @override
  final int id;
  @override
  @JsonKey(name: 'track_number')
  final String trackNumber;
  @override
  @JsonKey(name: 'description')
  final String? description;
  @override
  @JsonKey(name: 'updated_at')
  final String? updatedAt;
  @override
  @JsonKey(name: 'created_at')
  final String? createdAt;
  @override
  final MyDeliveryStageModel? stage;

  @override
  String toString() {
    return 'MyDeliveryModel(id: $id, trackNumber: $trackNumber, description: $description, updatedAt: $updatedAt, createdAt: $createdAt, stage: $stage)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$MyDeliveryModelImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.trackNumber, trackNumber) ||
                other.trackNumber == trackNumber) &&
            (identical(other.description, description) ||
                other.description == description) &&
            (identical(other.updatedAt, updatedAt) ||
                other.updatedAt == updatedAt) &&
            (identical(other.createdAt, createdAt) ||
                other.createdAt == createdAt) &&
            (identical(other.stage, stage) || other.stage == stage));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType, id, trackNumber, description, updatedAt, createdAt, stage);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$MyDeliveryModelImplCopyWith<_$MyDeliveryModelImpl> get copyWith =>
      __$$MyDeliveryModelImplCopyWithImpl<_$MyDeliveryModelImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$MyDeliveryModelImplToJson(
      this,
    );
  }
}

abstract class _MyDeliveryModel implements MyDeliveryModel {
  const factory _MyDeliveryModel(
      {required final int id,
      @JsonKey(name: 'track_number') required final String trackNumber,
      @JsonKey(name: 'description') final String? description,
      @JsonKey(name: 'updated_at') final String? updatedAt,
      @JsonKey(name: 'created_at') final String? createdAt,
      final MyDeliveryStageModel? stage}) = _$MyDeliveryModelImpl;

  factory _MyDeliveryModel.fromJson(Map<String, dynamic> json) =
      _$MyDeliveryModelImpl.fromJson;

  @override
  int get id;
  @override
  @JsonKey(name: 'track_number')
  String get trackNumber;
  @override
  @JsonKey(name: 'description')
  String? get description;
  @override
  @JsonKey(name: 'updated_at')
  String? get updatedAt;
  @override
  @JsonKey(name: 'created_at')
  String? get createdAt;
  @override
  MyDeliveryStageModel? get stage;
  @override
  @JsonKey(ignore: true)
  _$$MyDeliveryModelImplCopyWith<_$MyDeliveryModelImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
