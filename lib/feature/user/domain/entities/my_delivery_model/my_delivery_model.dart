// ignore_for_file: invalid_annotation_target

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:optoffka/feature/user/domain/entities/my_delivery_stage_model/my_delivery_stage_model.dart';

part 'my_delivery_model.freezed.dart';
part 'my_delivery_model.g.dart';

@freezed
class MyDeliveryModel with _$MyDeliveryModel {
  const factory MyDeliveryModel({
    required int id,
    @JsonKey(name: 'track_number') required String trackNumber,
    @JsonKey(name: 'description') String? description,
    @JsonKey(name: 'updated_at') String? updatedAt,
    @JsonKey(name: 'created_at') String? createdAt,
    MyDeliveryStageModel? stage,
  }) = _MyDeliveryModel;
  factory MyDeliveryModel.fromJson(Map<String, dynamic> json) =>
      _$MyDeliveryModelFromJson(json);
}
