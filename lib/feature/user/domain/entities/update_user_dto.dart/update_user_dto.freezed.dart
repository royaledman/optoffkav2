// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'update_user_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

UpdateUserDto _$UpdateUserDtoFromJson(Map<String, dynamic> json) {
  return _UpdateUserDto.fromJson(json);
}

/// @nodoc
mixin _$UpdateUserDto {
  String get name => throw _privateConstructorUsedError;
  String get city => throw _privateConstructorUsedError;
  String get phone => throw _privateConstructorUsedError;
  String get password => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $UpdateUserDtoCopyWith<UpdateUserDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UpdateUserDtoCopyWith<$Res> {
  factory $UpdateUserDtoCopyWith(
          UpdateUserDto value, $Res Function(UpdateUserDto) then) =
      _$UpdateUserDtoCopyWithImpl<$Res, UpdateUserDto>;
  @useResult
  $Res call({String name, String city, String phone, String password});
}

/// @nodoc
class _$UpdateUserDtoCopyWithImpl<$Res, $Val extends UpdateUserDto>
    implements $UpdateUserDtoCopyWith<$Res> {
  _$UpdateUserDtoCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? city = null,
    Object? phone = null,
    Object? password = null,
  }) {
    return _then(_value.copyWith(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      city: null == city
          ? _value.city
          : city // ignore: cast_nullable_to_non_nullable
              as String,
      phone: null == phone
          ? _value.phone
          : phone // ignore: cast_nullable_to_non_nullable
              as String,
      password: null == password
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$UpdateUserDtoImplCopyWith<$Res>
    implements $UpdateUserDtoCopyWith<$Res> {
  factory _$$UpdateUserDtoImplCopyWith(
          _$UpdateUserDtoImpl value, $Res Function(_$UpdateUserDtoImpl) then) =
      __$$UpdateUserDtoImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String name, String city, String phone, String password});
}

/// @nodoc
class __$$UpdateUserDtoImplCopyWithImpl<$Res>
    extends _$UpdateUserDtoCopyWithImpl<$Res, _$UpdateUserDtoImpl>
    implements _$$UpdateUserDtoImplCopyWith<$Res> {
  __$$UpdateUserDtoImplCopyWithImpl(
      _$UpdateUserDtoImpl _value, $Res Function(_$UpdateUserDtoImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? city = null,
    Object? phone = null,
    Object? password = null,
  }) {
    return _then(_$UpdateUserDtoImpl(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      city: null == city
          ? _value.city
          : city // ignore: cast_nullable_to_non_nullable
              as String,
      phone: null == phone
          ? _value.phone
          : phone // ignore: cast_nullable_to_non_nullable
              as String,
      password: null == password
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$UpdateUserDtoImpl implements _UpdateUserDto {
  _$UpdateUserDtoImpl(
      {required this.name,
      required this.city,
      required this.phone,
      required this.password});

  factory _$UpdateUserDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$UpdateUserDtoImplFromJson(json);

  @override
  final String name;
  @override
  final String city;
  @override
  final String phone;
  @override
  final String password;

  @override
  String toString() {
    return 'UpdateUserDto(name: $name, city: $city, phone: $phone, password: $password)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UpdateUserDtoImpl &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.city, city) || other.city == city) &&
            (identical(other.phone, phone) || other.phone == phone) &&
            (identical(other.password, password) ||
                other.password == password));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, name, city, phone, password);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$UpdateUserDtoImplCopyWith<_$UpdateUserDtoImpl> get copyWith =>
      __$$UpdateUserDtoImplCopyWithImpl<_$UpdateUserDtoImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$UpdateUserDtoImplToJson(
      this,
    );
  }
}

abstract class _UpdateUserDto implements UpdateUserDto {
  factory _UpdateUserDto(
      {required final String name,
      required final String city,
      required final String phone,
      required final String password}) = _$UpdateUserDtoImpl;

  factory _UpdateUserDto.fromJson(Map<String, dynamic> json) =
      _$UpdateUserDtoImpl.fromJson;

  @override
  String get name;
  @override
  String get city;
  @override
  String get phone;
  @override
  String get password;
  @override
  @JsonKey(ignore: true)
  _$$UpdateUserDtoImplCopyWith<_$UpdateUserDtoImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
