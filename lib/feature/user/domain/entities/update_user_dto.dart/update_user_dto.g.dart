// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'update_user_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$UpdateUserDtoImpl _$$UpdateUserDtoImplFromJson(Map<String, dynamic> json) =>
    _$UpdateUserDtoImpl(
      name: json['name'] as String,
      city: json['city'] as String,
      phone: json['phone'] as String,
      password: json['password'] as String,
    );

Map<String, dynamic> _$$UpdateUserDtoImplToJson(_$UpdateUserDtoImpl instance) =>
    <String, dynamic>{
      'name': instance.name,
      'city': instance.city,
      'phone': instance.phone,
      'password': instance.password,
    };
