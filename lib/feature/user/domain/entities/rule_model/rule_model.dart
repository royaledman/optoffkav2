import 'package:freezed_annotation/freezed_annotation.dart';

part 'rule_model.g.dart';
part 'rule_model.freezed.dart';

@freezed
class RuleModel with _$RuleModel{
  const factory RuleModel({
  required String conditions,
  required String shipment,
  required String regions,
  required String storage,
  }) = _RuleModel;

  factory RuleModel.fromJson(Map<String, dynamic> json) => _$RuleModelFromJson(json);
}