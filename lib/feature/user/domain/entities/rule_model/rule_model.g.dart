// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rule_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$RuleModelImpl _$$RuleModelImplFromJson(Map<String, dynamic> json) =>
    _$RuleModelImpl(
      conditions: json['conditions'] as String,
      shipment: json['shipment'] as String,
      regions: json['regions'] as String,
      storage: json['storage'] as String,
    );

Map<String, dynamic> _$$RuleModelImplToJson(_$RuleModelImpl instance) =>
    <String, dynamic>{
      'conditions': instance.conditions,
      'shipment': instance.shipment,
      'regions': instance.regions,
      'storage': instance.storage,
    };
