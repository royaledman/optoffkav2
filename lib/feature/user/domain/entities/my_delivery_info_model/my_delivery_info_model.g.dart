// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'my_delivery_info_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$MyDeliveryInfoModelImpl _$$MyDeliveryInfoModelImplFromJson(
        Map<String, dynamic> json) =>
    _$MyDeliveryInfoModelImpl(
      description: json['description'] as String?,
      trackNumber: json['track_number'] as String,
    );

Map<String, dynamic> _$$MyDeliveryInfoModelImplToJson(
        _$MyDeliveryInfoModelImpl instance) =>
    <String, dynamic>{
      'description': instance.description,
      'track_number': instance.trackNumber,
    };
