// ignore_for_file: invalid_annotation_target

import 'package:freezed_annotation/freezed_annotation.dart';

part 'my_delivery_info_model.freezed.dart';
part 'my_delivery_info_model.g.dart';

@freezed
class MyDeliveryInfoModel with _$MyDeliveryInfoModel {
  const factory MyDeliveryInfoModel({
    String? description,
    @JsonKey(name: 'track_number') required String trackNumber,
  }) = _MyDeliveryInfoModel;
  factory MyDeliveryInfoModel.fromJson(Map<String, dynamic> json) =>
      _$MyDeliveryInfoModelFromJson(json);
}
