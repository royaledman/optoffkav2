// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'my_delivery_info_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

MyDeliveryInfoModel _$MyDeliveryInfoModelFromJson(Map<String, dynamic> json) {
  return _MyDeliveryInfoModel.fromJson(json);
}

/// @nodoc
mixin _$MyDeliveryInfoModel {
  String? get description => throw _privateConstructorUsedError;
  @JsonKey(name: 'track_number')
  String get trackNumber => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $MyDeliveryInfoModelCopyWith<MyDeliveryInfoModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MyDeliveryInfoModelCopyWith<$Res> {
  factory $MyDeliveryInfoModelCopyWith(
          MyDeliveryInfoModel value, $Res Function(MyDeliveryInfoModel) then) =
      _$MyDeliveryInfoModelCopyWithImpl<$Res, MyDeliveryInfoModel>;
  @useResult
  $Res call(
      {String? description, @JsonKey(name: 'track_number') String trackNumber});
}

/// @nodoc
class _$MyDeliveryInfoModelCopyWithImpl<$Res, $Val extends MyDeliveryInfoModel>
    implements $MyDeliveryInfoModelCopyWith<$Res> {
  _$MyDeliveryInfoModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? description = freezed,
    Object? trackNumber = null,
  }) {
    return _then(_value.copyWith(
      description: freezed == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      trackNumber: null == trackNumber
          ? _value.trackNumber
          : trackNumber // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$MyDeliveryInfoModelImplCopyWith<$Res>
    implements $MyDeliveryInfoModelCopyWith<$Res> {
  factory _$$MyDeliveryInfoModelImplCopyWith(_$MyDeliveryInfoModelImpl value,
          $Res Function(_$MyDeliveryInfoModelImpl) then) =
      __$$MyDeliveryInfoModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String? description, @JsonKey(name: 'track_number') String trackNumber});
}

/// @nodoc
class __$$MyDeliveryInfoModelImplCopyWithImpl<$Res>
    extends _$MyDeliveryInfoModelCopyWithImpl<$Res, _$MyDeliveryInfoModelImpl>
    implements _$$MyDeliveryInfoModelImplCopyWith<$Res> {
  __$$MyDeliveryInfoModelImplCopyWithImpl(_$MyDeliveryInfoModelImpl _value,
      $Res Function(_$MyDeliveryInfoModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? description = freezed,
    Object? trackNumber = null,
  }) {
    return _then(_$MyDeliveryInfoModelImpl(
      description: freezed == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      trackNumber: null == trackNumber
          ? _value.trackNumber
          : trackNumber // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$MyDeliveryInfoModelImpl implements _MyDeliveryInfoModel {
  const _$MyDeliveryInfoModelImpl(
      {this.description,
      @JsonKey(name: 'track_number') required this.trackNumber});

  factory _$MyDeliveryInfoModelImpl.fromJson(Map<String, dynamic> json) =>
      _$$MyDeliveryInfoModelImplFromJson(json);

  @override
  final String? description;
  @override
  @JsonKey(name: 'track_number')
  final String trackNumber;

  @override
  String toString() {
    return 'MyDeliveryInfoModel(description: $description, trackNumber: $trackNumber)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$MyDeliveryInfoModelImpl &&
            (identical(other.description, description) ||
                other.description == description) &&
            (identical(other.trackNumber, trackNumber) ||
                other.trackNumber == trackNumber));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, description, trackNumber);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$MyDeliveryInfoModelImplCopyWith<_$MyDeliveryInfoModelImpl> get copyWith =>
      __$$MyDeliveryInfoModelImplCopyWithImpl<_$MyDeliveryInfoModelImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$MyDeliveryInfoModelImplToJson(
      this,
    );
  }
}

abstract class _MyDeliveryInfoModel implements MyDeliveryInfoModel {
  const factory _MyDeliveryInfoModel(
          {final String? description,
          @JsonKey(name: 'track_number') required final String trackNumber}) =
      _$MyDeliveryInfoModelImpl;

  factory _MyDeliveryInfoModel.fromJson(Map<String, dynamic> json) =
      _$MyDeliveryInfoModelImpl.fromJson;

  @override
  String? get description;
  @override
  @JsonKey(name: 'track_number')
  String get trackNumber;
  @override
  @JsonKey(ignore: true)
  _$$MyDeliveryInfoModelImplCopyWith<_$MyDeliveryInfoModelImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
