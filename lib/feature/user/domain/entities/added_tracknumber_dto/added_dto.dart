// ignore_for_file: invalid_annotation_target

import 'package:freezed_annotation/freezed_annotation.dart';

part 'added_dto.freezed.dart';
part 'added_dto.g.dart';

@freezed
class AddedDto with _$AddedDto {
  factory AddedDto({
    @JsonKey(name: 'track_number') required String trackNumber,
    required String description,
  }) = _AddedDto;

  factory AddedDto.fromJson(Map<String, dynamic> json) =>
      _$AddedDtoFromJson(json);
}
