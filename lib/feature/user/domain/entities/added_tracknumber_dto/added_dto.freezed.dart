// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'added_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

AddedDto _$AddedDtoFromJson(Map<String, dynamic> json) {
  return _AddedDto.fromJson(json);
}

/// @nodoc
mixin _$AddedDto {
  @JsonKey(name: 'track_number')
  String get trackNumber => throw _privateConstructorUsedError;
  String get description => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AddedDtoCopyWith<AddedDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AddedDtoCopyWith<$Res> {
  factory $AddedDtoCopyWith(AddedDto value, $Res Function(AddedDto) then) =
      _$AddedDtoCopyWithImpl<$Res, AddedDto>;
  @useResult
  $Res call(
      {@JsonKey(name: 'track_number') String trackNumber, String description});
}

/// @nodoc
class _$AddedDtoCopyWithImpl<$Res, $Val extends AddedDto>
    implements $AddedDtoCopyWith<$Res> {
  _$AddedDtoCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? trackNumber = null,
    Object? description = null,
  }) {
    return _then(_value.copyWith(
      trackNumber: null == trackNumber
          ? _value.trackNumber
          : trackNumber // ignore: cast_nullable_to_non_nullable
              as String,
      description: null == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$AddedDtoImplCopyWith<$Res>
    implements $AddedDtoCopyWith<$Res> {
  factory _$$AddedDtoImplCopyWith(
          _$AddedDtoImpl value, $Res Function(_$AddedDtoImpl) then) =
      __$$AddedDtoImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: 'track_number') String trackNumber, String description});
}

/// @nodoc
class __$$AddedDtoImplCopyWithImpl<$Res>
    extends _$AddedDtoCopyWithImpl<$Res, _$AddedDtoImpl>
    implements _$$AddedDtoImplCopyWith<$Res> {
  __$$AddedDtoImplCopyWithImpl(
      _$AddedDtoImpl _value, $Res Function(_$AddedDtoImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? trackNumber = null,
    Object? description = null,
  }) {
    return _then(_$AddedDtoImpl(
      trackNumber: null == trackNumber
          ? _value.trackNumber
          : trackNumber // ignore: cast_nullable_to_non_nullable
              as String,
      description: null == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$AddedDtoImpl implements _AddedDto {
  _$AddedDtoImpl(
      {@JsonKey(name: 'track_number') required this.trackNumber,
      required this.description});

  factory _$AddedDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$AddedDtoImplFromJson(json);

  @override
  @JsonKey(name: 'track_number')
  final String trackNumber;
  @override
  final String description;

  @override
  String toString() {
    return 'AddedDto(trackNumber: $trackNumber, description: $description)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AddedDtoImpl &&
            (identical(other.trackNumber, trackNumber) ||
                other.trackNumber == trackNumber) &&
            (identical(other.description, description) ||
                other.description == description));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, trackNumber, description);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$AddedDtoImplCopyWith<_$AddedDtoImpl> get copyWith =>
      __$$AddedDtoImplCopyWithImpl<_$AddedDtoImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$AddedDtoImplToJson(
      this,
    );
  }
}

abstract class _AddedDto implements AddedDto {
  factory _AddedDto(
      {@JsonKey(name: 'track_number') required final String trackNumber,
      required final String description}) = _$AddedDtoImpl;

  factory _AddedDto.fromJson(Map<String, dynamic> json) =
      _$AddedDtoImpl.fromJson;

  @override
  @JsonKey(name: 'track_number')
  String get trackNumber;
  @override
  String get description;
  @override
  @JsonKey(ignore: true)
  _$$AddedDtoImplCopyWith<_$AddedDtoImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
