// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'added_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$AddedDtoImpl _$$AddedDtoImplFromJson(Map<String, dynamic> json) =>
    _$AddedDtoImpl(
      trackNumber: json['track_number'] as String,
      description: json['description'] as String,
    );

Map<String, dynamic> _$$AddedDtoImplToJson(_$AddedDtoImpl instance) =>
    <String, dynamic>{
      'track_number': instance.trackNumber,
      'description': instance.description,
    };
