import 'package:optoffka/feature/user/domain/entities/added_tracknumber_dto/added_dto.dart';
import 'package:optoffka/feature/user/domain/entities/banner_model/banner_model.dart';
import 'package:optoffka/feature/user/domain/entities/history_model/history_model.dart';
import 'package:optoffka/feature/user/domain/entities/login_dto/login_dto.dart';
import 'package:optoffka/feature/user/domain/entities/my_delivery_info_model/my_delivery_info_model.dart';
import 'package:optoffka/feature/user/domain/entities/my_delivery_model/my_delivery_model.dart';
import 'package:optoffka/feature/user/domain/entities/notification_model/notification_model.dart';
import 'package:optoffka/feature/user/domain/entities/receipts_model/receipts_model.dart';
import 'package:optoffka/feature/user/domain/entities/rule_model/rule_model.dart';
import 'package:optoffka/feature/user/domain/entities/update_user_dto.dart/update_user_dto.dart';
import 'package:optoffka/feature/user/domain/entities/user_model/user_model.dart';

abstract class UserRepository {
  Future<int> login({
    required LoginDto dto,
  });

  Future<UserModel> getUser();

  Future<List<BannerModel>> getBanner();

  Future<List<NotificationModel>> getNotificationBanner();
  Future<List<MyDeliveryModel>> getMyDelivery({
    String? stageId,
    String? description,
  });
  Future<List<RuleModel>> getRule();
  Future<List<ReceiptsModel>> receiptsModel();

  Future<HistoryModel> getHistory({
    required String ship,
  });

  Future<void> logout();

  Future<void> updateProfile({
    required UpdateUserDto dto,
  });

  Future<void> createTrack({
    required AddedDto dto,
  });

  Future<MyDeliveryInfoModel> getMyDeliveryInfo({
    required int trackId,
  });

  Future<void> deleteDelivery({
    required int trackId,
  });

  Future<List<MyDeliveryModel>> searchTrackNumber({String? search});

  Future<void> updateShipDescription({
    required int shipId,
    required String description,
  });
}
